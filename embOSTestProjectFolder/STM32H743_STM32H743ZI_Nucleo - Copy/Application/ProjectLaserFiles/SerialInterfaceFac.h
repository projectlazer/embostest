
#pragma once
#include "SerialInterFaceDriver.h" 
#include <map>
class SerialInterfaceFac {

private:
  typedef std::map<SerialInterFaceType, SerialInterFaceDriver*> SerialInterFaceMap; 

public:
static SerialInterFaceMap& getInterfaceMap();

static SerialInterFaceDriver* makeInterface( SerialInterFaceType type);
static void addInterface(SerialInterFaceType type, SerialInterFaceDriver* interface);



};