#include "LaserFac.h"

void LaserFac::choseLaser(LaserDriveType type,LaserDriver *laserdriver) {

  LaserDriverMap& temp = getLaserMap();
  temp.insert(std::pair<LaserDriveType,LaserDriver*>(type,laserdriver));

}

LaserDriver *LaserFac::makelaser(LaserDriveType type) {
  //LaserDriver* tempLaserDriver = getLaserMap();
  LaserDriverMap& lookup = getLaserMap();
  LaserDriver* temp = lookup.find(type)->second->clone();
  return temp;

}

LaserFac::LaserDriverMap& LaserFac::getLaserMap() {

  static LaserDriverMap returntype;
  return returntype;
}