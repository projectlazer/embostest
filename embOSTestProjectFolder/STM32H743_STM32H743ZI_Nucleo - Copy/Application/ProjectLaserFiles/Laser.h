#pragma  once
#include "LaserStructs.h" 
#include "LaserDriver.h"

#include "LaserFac.h"

//De Laser klasse is de interface naar de LaserDriver toe.
// de "gebruiker"van de laser hoeft niet te weten hoe de laser aangestuurt word.
//Het einige wat nodig is is welke coordianaat waneer getekent moet worden
class Laser {

public:
  Laser();
  Laser(LaserDriveType);
  Laser(LaserDriver *driver);
  virtual ~Laser();
  void writeCoord(LaserPos pos);
private:
  LaserDriver *Driver;
  LaserCoord Coord;
  uint32_t updateSpeed;
};