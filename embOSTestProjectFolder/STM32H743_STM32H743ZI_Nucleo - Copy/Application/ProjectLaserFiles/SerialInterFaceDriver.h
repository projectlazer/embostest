
#pragma  once
#include "SerialInterfaceEnum.h"
//#include "SerialInterfaceFac.h"
#include "stdint.h"
class SerialInterFaceDriver{
public:

SerialInterFaceDriver(SerialInterFaceType type);


virtual ~SerialInterFaceDriver();

virtual void writeData(uint32_t addres, uint8_t *data, uint32_t count)=0; //32bit count
virtual void writeData(uint32_t addres, uint16_t *data,uint32_t count)=0;
virtual void writeData(uint32_t addres, uint32_t *data,uint32_t count)=0;
virtual void* ReadData(uint32_t addres,uint32_t *data,uint32_t count)=0; //lezen word altijd naar een 32 bit data pointer gedaan. houd er rekening mee dat als de HAL driver 8 bit leest dat er 4 keer data in het 32 bit staat
virtual SerialInterFaceDriver* clone()=0;
virtual void addHalDriver( void* HalDriver)=0;

private:
SerialInterFaceDriver *Driver;


};