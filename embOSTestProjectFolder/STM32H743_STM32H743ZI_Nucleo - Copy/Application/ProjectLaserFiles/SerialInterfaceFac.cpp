
#include "SerialInterfaceFac.h"

SerialInterfaceFac::SerialInterFaceMap &SerialInterfaceFac::getInterfaceMap() {
  static SerialInterFaceMap map;
  return map;
}

SerialInterFaceDriver *SerialInterfaceFac::makeInterface(SerialInterFaceType type) {
  SerialInterFaceMap &lookup = getInterfaceMap();
  SerialInterFaceDriver *temp = lookup.find(type)->second->clone();
  return temp;
}
void SerialInterfaceFac::addInterface(SerialInterFaceType type, SerialInterFaceDriver *interface) {
  SerialInterFaceMap &lookup = getInterfaceMap();
  lookup.insert(std::pair<SerialInterFaceType, SerialInterFaceDriver *>(type, interface));
}