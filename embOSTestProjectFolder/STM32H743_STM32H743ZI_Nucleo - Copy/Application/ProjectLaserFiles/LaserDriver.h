#pragma once
#include "DacDriver.h"
#include "LaserStructs.h"
#include "DriverEnum.h" 

//LaserDriver is het gene wat de laser gebruikt.
//De LaserDriver bied een Manier van aansturen van een Laser aan
//Als er zo een laser gebruikt word dat UART ipv SPI gebruikt dan kan er een nieuwe afgeleide klasse gemaakt worden die UART gebruikt
class LaserDriver {

public:
  LaserDriver();
  LaserDriver(LaserDriveType DriverType);
  virtual ~LaserDriver();
  virtual void writeValue(LaserPos laserpos, uint32_t timetopoint) = 0;
  virtual void writeColor(LaserColor Color) = 0;
  virtual void writePoint(LaserCoord coord) = 0;
  virtual void SetTimetoPoint(uint32_t timetoPoint) = 0;
  virtual LaserDriver* clone()=0;
private:
  DacDriver dac;
};