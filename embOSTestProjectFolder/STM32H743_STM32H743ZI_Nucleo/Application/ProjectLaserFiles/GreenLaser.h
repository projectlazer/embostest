#pragma once
#include "LaserDriver.h" 
class GreenLaser : public LaserDriver{
public:
GreenLaser();
virtual ~GreenLaser();
 void writeValue(LaserPos laserpos, uint32_t timetopoint);
 void writeColor(LaserColor Color);
 void writePoint(LaserCoord coord);
 void SetTimetoPoint(uint32_t timetoPoint);
  GreenLaser* clone();
private:
static GreenLaser Own;

};