
#include "SerialInterFaceDriver.h"
#include "stm32h7xx_hal.h"
struct SlaveSelectStruct {
  GPIO_TypeDef *GPIOHanndle;
  uint16_t GPIO_Pin;
  uint8_t slaveactivestate : 1;
};

struct SPIANDGPIOstruct {
  SPI_HandleTypeDef *SpiHandle;
  SlaveSelectStruct SlaceSelectArray[8];
};

class SPIInterface : public SerialInterFaceDriver {
  //als de hal driver gebruikt word maken de verschillende writedata's uit.
  //addres is in dit geval de chipselect
public:
  SPIInterface();
  virtual ~SPIInterface();

  void writeData(uint32_t addres, uint8_t *data, uint32_t count); //32bit count
  void writeData(uint32_t addres, uint16_t *data, uint32_t count);
  void writeData(uint32_t addres, uint32_t *data, uint32_t count);
  void* ReadData(uint32_t addres, uint32_t *data, uint32_t count); //lezen word altijd naar een 32 bit data pointer gedaan.
  SPIInterface *clone();
  void addHalDriver(void *HalDriver);
  void testfucntion();

private:
  SPIANDGPIOstruct *testStruct = nullptr;

  SPI_HandleTypeDef *SpiHandle = nullptr;
  static SPIInterface Own;
};