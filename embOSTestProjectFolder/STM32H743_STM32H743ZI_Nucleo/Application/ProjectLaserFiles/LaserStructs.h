#include "stdint.h"
#pragma once
 typedef struct LaserPos {
    uint16_t x;
    uint16_t y;
  };

  typedef struct LaserColor {

    uint8_t red;
    uint8_t green;
    uint8_t blue;

  };

  typedef struct LaserCoord {

    LaserPos Pos;
    LaserColor Color;
  };