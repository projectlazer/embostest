#include "SPIInterface.h"
SPIInterface SPIInterface::Own;
SPIInterface::SPIInterface() : SerialInterFaceDriver(SPI) {

  // geen init nodig omdat de HAL drivers gebruikt worden
}

void SPIInterface::writeData(uint32_t addres, uint8_t *data, uint32_t count) {
}
void SPIInterface::writeData(uint32_t addres, uint16_t *data, uint32_t count) {
 SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Transmit(testStruct->SpiHandle, (uint8_t *)data, count, 100);
 HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);



}
void SPIInterface::writeData(uint32_t addres, uint32_t *data, uint32_t count) {
  //TODO kijken of dit mogelijk met een decorator kan gedaan worden
  //TODO voeg mogelijk een check toe voor de spihandle voor als er geen HAL drivers gebruikt worden
  //if(SpiHandle != nullptr){

  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Transmit(testStruct->SpiHandle, (uint8_t *)data, count, 100);
  HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
}

void* SPIInterface::ReadData(uint32_t addres, uint32_t *data, uint32_t count) {

  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Receive(testStruct->SpiHandle, (uint8_t *)data, count, 100);
  HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
  return 0;
}

SPIInterface::~SPIInterface() {
}
SPIInterface *SPIInterface::clone() {
  return new SPIInterface;
}
void SPIInterface::addHalDriver(void *HalDriver) {
  testStruct = (SPIANDGPIOstruct *)HalDriver;
}