
#include "NucleaoLeds.h"

#define LD1_PIN (0)  // LD1, Green Led
#define LD2_PIN (7)  // LD2, Blue Led
#define LD3_PIN (14) // LD3, Red Led
#define LaserPinR (2)
#define LaserPinG (1)
#define LaserPinB (0)
#define ButtonPin (13)

#define DACSELECT (14)

#define BUTTONMIDDLE (9)
#define BUTTONLEFT (7)
#define BUTTONRIGHT (8)
#define BUTTONUP (5)
#define BUTTONDOWN (6)





#define GPIOB_BASE_ADDR (0x58020400u)
#define GPIOB_MODER (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x00))
#define GPIOB_OTYPER (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x04))
#define GPIOB_OSPEEDR (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x08))
#define GPIOB_PUPDR (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x0C))
#define GPIOB_ODR (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x14))
#define GPIOB_BSRR (*(volatile unsigned int *)(GPIOB_BASE_ADDR + 0x18))

#define GPIOG_BASE_ADDR (0x58021800)
#define GPIOG_MODER (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x00))
#define GPIOG_OTYPER (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x04))
#define GPIOG_OSPEEDR (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x08))
#define GPIOG_PUPDR (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x0C))
#define GPIOG_ODR (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x14))
#define GPIOG_BSRR (*(volatile unsigned int *)(GPIOG_BASE_ADDR + 0x18))

#define GPIOC_BASE_ADDR (0x58020800)
#define GPIOC_MODER (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x00))
#define GPIOC_OTYPER (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x04))
#define GPIOC_OSPEEDR (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x08))
#define GPIOC_PUPDR (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x0C))
#define GPIOC_ODR (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x14))
#define GPIOC_BSRR (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x18))
#define GPIOC_IDR (*(volatile unsigned int *)(GPIOC_BASE_ADDR + 0x10))

#define GPIOA_BASE_ADDR (0x58020000)
#define GPIOA_MODER (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x00))
#define GPIOA_OTYPER (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x04))
#define GPIOA_OSPEEDR (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x08))
#define GPIOA_PUPDR (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x0C))
#define GPIOA_ODR (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x14))
#define GPIOA_BSRR (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x18))
#define GPIOA_IDR (*(volatile unsigned int *)(GPIOA_BASE_ADDR + 0x10))

#define GPIOE_BASE_ADDR (0x58021000)
#define GPIOE(x) (*(volatile unsigned int *) (GPIOE_BASE_ADDR+x))

#define GPIOE_MODER (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x00))
#define GPIOE_OTYPER (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x04))
#define GPIOE_OSPEEDR (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x08))
#define GPIOE_PUPDR (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x0C))
#define GPIOE_ODR (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x14))
#define GPIOE_BSRR (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x18))
#define GPIOE_IDR (*(volatile unsigned int *)(GPIOE_BASE_ADDR + 0x10))

#define GPIOG_BASE_ADDR (0x58021800)
#define GPIOG(x) (*(volatile unsigned int *) (GPIOG_BASE_ADDR+x))

#define GPIOF_BASE_ADDR (0x58021400)
#define GPIOF(x) (*(volatile unsigned int *) (GPIOF_BASE_ADDR+x))




#define GPIOD_BASE_ADDR (0x58020C00)
#define GPIOD(x) (*(volatile unsigned int *) (GPIOD_BASE_ADDR +x))


#define PLUS15VGPIO(x) GPIOG(x)
#define PLUS15VPIN   (14)
#define PLUS15ONSTATE (0)


#define PLUS3V3GPIO(x) GPIOG(x)
#define PLUS3V3PIN  (13)
#define PLUS3V3ONSTATE (0)

#define MINUS15VGPIO(x) GPIOG(x)
#define MINUS15VPIN  (15)
#define MINUS15ONSTATE (0)

#define PLUS12VGPIO(x)  GPIOD(x) 
#define PLUS12VPIN  (6)
#define PLUS12ONSTATE (0)

#define PLUS5VGPIO(x)   GPIOD(x) 
#define PLUS5VPIN   (7)
#define PLUS5ONSTATE (0)


#define UPBUTTONGPIO(x)     GPIOE(x) 
#define UPBUTTONPIN         (2)

#define DOWNBUTTONGPIO(x)   GPIOE(x) 
#define DOWNBUTTONPIN       (3)

#define LEFTBUTTONGPIO(x)   GPIOE(x) 
#define LEFTBUTTONPIN       (4)

#define RIGHTBUTTONGPIO(x)  GPIOE(x) 
#define RIGHTBUTTONPIN      (5)

#define MIDBUTTONGPIO(x)    GPIOE(x) 
#define MIDBUTTONPIN        (6)

#define ABUTTONGPIO(x)      GPIOF(x) 
#define ABUTTONPIN          (0)

#define BBUTTONGPIO(x)      GPIOF(x) 
#define BBUTTONPIN          (1)






            
#define GPIOD_MODER (*(volatile unsigned int *)   (GPIOD_BASE_ADDR + 0x00))
#define GPIOD_OTYPER (*(volatile unsigned int *)  (GPIOD_BASE_ADDR + 0x04))
#define GPIOD_OSPEEDR (*(volatile unsigned int *) (GPIOD_BASE_ADDR + 0x08))
#define GPIOD_PUPDR (*(volatile unsigned int *)   (GPIOD_BASE_ADDR + 0x0C))
#define GPIOD_ODR (*(volatile unsigned int *)     (GPIOD_BASE_ADDR + 0x14))
#define GPIOD_BSRR (*(volatile unsigned int *)    (GPIOD_BASE_ADDR + 0x18))
#define GPIOD_IDR (*(volatile unsigned int *)     (GPIOD_BASE_ADDR + 0x10))





#define MODER     0x00
#define OTYPER    0x04
#define OSPEEDR   0x08
#define PUPDR     0x0C
#define ODR       0x14
#define BSRR      0x18
#define IDR       0x10







#define RCC_BASE_ADDR (0x58024400U)
#define RCC_AHB4ENR (*(volatile unsigned int *)(RCC_BASE_ADDR + 0xE0))

void BSP_Init() {
  //
  // Enable GPIO clocks
  //
  RCC_AHB4ENR |= (0x01u << 1); // Enable the GPIOB clock
  //
  // Initialize LD1
  //
  GPIOB_MODER &= ~(0x3u << (LD1_PIN * 2));  // Clear mode register
  GPIOB_MODER |= (0x1u << (LD1_PIN * 2));   // Set IO direction to output mode
  GPIOB_OSPEEDR |= (0x3u << (LD1_PIN * 2)); // Set speed to high frequency
  GPIOB_OTYPER &= ~(0x1u << (LD1_PIN * 1)); // Set output to push-pull
  GPIOB_PUPDR &= ~(0x3u << (LD1_PIN * 2));  // Clear the pull-up/pull-down register
  GPIOB_PUPDR |= (0x1u << (LD1_PIN * 2));   // Set push-pull to pull-up
  GPIOB_BSRR = ((0x1u << 16) << LD1_PIN);   // Turn LED off
  //
  // Initialize LD2
  //
  GPIOB_MODER &= ~(0x3u << (LD2_PIN * 2));  // Clear mode register
  GPIOB_MODER |= (0x1u << (LD2_PIN * 2));   // Set IO direction to output mode
  GPIOB_OSPEEDR |= (0x3u << (LD2_PIN * 2)); // Set speed to high frequency
  GPIOB_OTYPER &= ~(0x1u << (LD2_PIN * 1)); // Set output to push-pull
  GPIOB_PUPDR &= ~(0x3u << (LD2_PIN * 2));  // Clear the pull-up/pull-down register
  GPIOB_PUPDR |= (0x1u << (LD2_PIN * 2));   // Set push-pull to pull-up
  GPIOB_BSRR = ((0x1u << 16) << LD2_PIN);   // Turn LED off
  //
  // Initialize LD3
  //
  GPIOB_MODER &= ~(0x3u << (LD3_PIN * 2));  // Clear mode register
  GPIOB_MODER |= (0x1u << (LD3_PIN * 2));   // Set IO direction to output mode
  GPIOB_OSPEEDR |= (0x3u << (LD3_PIN * 2)); // Set speed to high frequency
  GPIOB_OTYPER &= ~(0x1u << (LD3_PIN * 1)); // Set output to push-pull
  GPIOB_PUPDR &= ~(0x3u << (LD3_PIN * 2));  // Clear the pull-up/pull-down register
  GPIOB_PUPDR |= (0x1u << (LD3_PIN * 2));   // Set push-pull to pull-up
  GPIOB_BSRR = ((0x1u << 16) << LD3_PIN);   // Turn LED off

  //laserEnable
  GPIOG_MODER &= ~(0x3u << (LaserPinR * 2));  // Clear mode register
  GPIOG_MODER |= (0x1u << (LaserPinR * 2));   // Set IO direction to output mode
  GPIOG_OSPEEDR |= (0x3u << (LaserPinR * 2)); // Set speed to high frequency
  GPIOG_OTYPER &= ~(0x1u << (LaserPinR * 1)); // Set output to push-pull
  GPIOG_PUPDR &= ~(0x3u << (LaserPinR * 2));  // Clear the pull-up/pull-down register
  GPIOG_PUPDR |= (0x1u << (LaserPinR * 2));   // Set push-pull to pull-up
  GPIOG_BSRR = ((0x1u << 16) << LaserPinR);   // Turn LED off

  GPIOG_MODER &= ~(0x3u << (LaserPinB * 2));  // Clear mode register
  GPIOG_MODER |= (0x1u << (LaserPinB * 2));   // Set IO direction to output mode
  GPIOG_OSPEEDR |= (0x3u << (LaserPinB * 2)); // Set speed to high frequency
  GPIOG_OTYPER &= ~(0x1u << (LaserPinB * 1)); // Set output to push-pull
  GPIOG_PUPDR &= ~(0x3u << (LaserPinB * 2));  // Clear the pull-up/pull-down register
  GPIOG_PUPDR |= (0x1u << (LaserPinB * 2));   // Set push-pull to pull-up
  GPIOG_BSRR = ((0x1u << 16) << LaserPinB);   // Turn LED off

  GPIOG_MODER &= ~(0x3u << (LaserPinG * 2));  // Clear mode register
  GPIOG_MODER |= (0x1u << (LaserPinG * 2));   // Set IO direction to output mode
  GPIOG_OSPEEDR |= (0x3u << (LaserPinG * 2)); // Set speed to high frequency
  GPIOG_OTYPER &= ~(0x1u << (LaserPinG * 1)); // Set output to push-pull
  GPIOG_PUPDR &= ~(0x3u << (LaserPinG * 2));  // Clear the pull-up/pull-down register
  GPIOG_PUPDR |= (0x1u << (LaserPinG * 2));   // Set push-pull to pull-up
  GPIOG_BSRR = ((0x1u << 16) << LaserPinG);   // Turn LED off

  //buttonEnable
  GPIOG_MODER &= ~(0x3u << (ButtonPin * 2)); // Zet button op input
  GPIOG_PUPDR &= ~(0x3u << (ButtonPin * 2)); // Clear the pull-up/pull-down register

  GPIOE_MODER &= ~(0x3u << (DACSELECT * 2)); // Zet button op input
  GPIOE_PUPDR &= ~(0x3u << (DACSELECT * 2)); // Clear the pull-up/pull-down register

  GPIOA_MODER &= ~(0x3u << (BUTTONMIDDLE * 2)); // Zet button op input
  GPIOA_PUPDR &= ~(0x3u << (BUTTONMIDDLE * 2)); // Clear the pull-up/pull-down register
   GPIOA_PUPDR |= (0x1u << (BUTTONMIDDLE * 2));
      
  GPIOA_MODER &= ~(0x3u << (BUTTONLEFT * 2)); // Zet button op input
  GPIOA_PUPDR &= ~(0x3u << (BUTTONLEFT * 2)); // Clear the pull-up/pull-down register
    GPIOA_PUPDR |= (0x1u << (BUTTONLEFT * 2));
      
  GPIOA_MODER &= ~(0x3u << (BUTTONRIGHT * 2)); // Zet button op input
  GPIOA_PUPDR &= ~(0x3u << (BUTTONRIGHT * 2)); // Clear the pull-up/pull-down register
    GPIOA_PUPDR |= (0x1u << (BUTTONRIGHT * 2));
      
  GPIOA_MODER &= ~(0x3u << (BUTTONDOWN * 2)); // Zet button op input
  GPIOA_PUPDR &= ~(0x3u << (BUTTONDOWN * 2)); // Clear the pull-up/pull-down register
     GPIOA_PUPDR |= (0x1u << (BUTTONDOWN * 2));
      
  GPIOA_MODER &= ~(0x3u << (BUTTONUP * 2)); // Zet button op input
  GPIOA_PUPDR &= ~(0x3u << (BUTTONUP * 2)); // Clear the pull-up/pull-down register
  GPIOA_PUPDR |= (0x1u << (BUTTONUP * 2));




  ABUTTONGPIO(MODER) &= ~(0x3u << (ABUTTONPIN * 2)); // Zet button op input
  ABUTTONGPIO(PUPDR) &= ~(0x3u << (ABUTTONPIN * 2)); // Clear the pull-up/pull-down register
  ABUTTONGPIO(PUPDR) |= (0x1u <<  (ABUTTONPIN * 2));
  
  BBUTTONGPIO(MODER) &= ~(0x3u << (BBUTTONPIN * 2)); // Zet button op input
  BBUTTONGPIO(PUPDR) &= ~(0x3u << (BBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  BBUTTONGPIO(PUPDR) |= (0x1u <<  (BBUTTONPIN * 2));

  MIDBUTTONGPIO(MODER) &= ~(0x3u << (MIDBUTTONPIN * 2)); // Zet button op input
  MIDBUTTONGPIO(PUPDR) &= ~(0x3u << (MIDBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  MIDBUTTONGPIO(PUPDR) |= (0x1u <<  (MIDBUTTONPIN * 2));

  UPBUTTONGPIO(MODER) &= ~(0x3u << (UPBUTTONPIN * 2)); // Zet button op input
  UPBUTTONGPIO(PUPDR) &= ~(0x3u << (UPBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  UPBUTTONGPIO(PUPDR) |= (0x1u <<  (UPBUTTONPIN * 2));

  LEFTBUTTONGPIO(MODER) &= ~(0x3u << (LEFTBUTTONPIN * 2)); // Zet button op input
  LEFTBUTTONGPIO(PUPDR) &= ~(0x3u << (LEFTBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  LEFTBUTTONGPIO(PUPDR) |= (0x1u <<  (LEFTBUTTONPIN * 2));

  RIGHTBUTTONGPIO(MODER) &= ~(0x3u << (RIGHTBUTTONPIN * 2)); // Zet button op input
  RIGHTBUTTONGPIO(PUPDR) &= ~(0x3u << (RIGHTBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  RIGHTBUTTONGPIO(PUPDR) |= (0x1u <<  (RIGHTBUTTONPIN * 2));

  DOWNBUTTONGPIO(MODER) &= ~(0x3u << (DOWNBUTTONPIN * 2)); // Zet button op input
  DOWNBUTTONGPIO(PUPDR) &= ~(0x3u << (DOWNBUTTONPIN * 2)); // Clear the pull-up/pull-down register
  DOWNBUTTONGPIO(PUPDR) |= (0x1u <<  (DOWNBUTTONPIN * 2));




  MINUS15VGPIO(MODER) &= ~(0x3u << (MINUS15VPIN * 2));  // Clear mode register
  MINUS15VGPIO(MODER) |= (0x1u << (MINUS15VPIN * 2));   // Set IO direction to output mode
  MINUS15VGPIO(OSPEEDR) |= (0x3u << (MINUS15VPIN * 2)); // Set speed to high frequency
  MINUS15VGPIO(OTYPER) &= ~(0x1u << (MINUS15VPIN * 1)); // Set output to push-pull
  MINUS15VGPIO(PUPDR) &= ~(0x3u << (MINUS15VPIN * 2));  // Clear the pull-up/pull-down register
  MINUS15VGPIO(PUPDR) |= (0x1u << (MINUS15VPIN * 2));   // Set push-pull to pull-up
   MINUS15VGPIO(ODR) ^= (-(unsigned long)MINUS15ONSTATE ^ MINUS15VGPIO(ODR)) & (1UL << MINUS15VPIN);


 PLUS15VGPIO(MODER) &= ~(0x3u <<   (PLUS15VPIN * 2));  // Clear mode register
 PLUS15VGPIO(MODER) |= (0x1u <<    (PLUS15VPIN * 2));   // Set IO direction to output mode
 PLUS15VGPIO(OSPEEDR) |= (0x3u <<  (PLUS15VPIN * 2)); // Set speed to high frequency
 PLUS15VGPIO(OTYPER) &= ~(0x1u <<  (PLUS15VPIN * 1)); // Set output to push-pull
 PLUS15VGPIO(PUPDR) &= ~(0x3u <<   (PLUS15VPIN * 2));  // Clear the pull-up/pull-down register
 PLUS15VGPIO(PUPDR) |= (0x1u <<    (PLUS15VPIN * 2));   // Set push-pull to pull-up
 PLUS15VGPIO(ODR) ^= (-(unsigned long)PLUS15ONSTATE ^ PLUS15VGPIO(ODR)) & (1UL << PLUS15VPIN);

  PLUS12VGPIO(MODER) &= ~(0x3u <<   (PLUS12VPIN * 2));  // Clear mode register
  PLUS12VGPIO(MODER) |= (0x1u <<    (PLUS12VPIN * 2));   // Set IO direction to output mode
  PLUS12VGPIO(OSPEEDR) |= (0x3u <<  (PLUS12VPIN * 2)); // Set speed to high frequency
  PLUS12VGPIO(OTYPER) &= ~(0x1u <<  (PLUS12VPIN * 1)); // Set output to push-pull
  PLUS12VGPIO(PUPDR) &= ~(0x3u <<   (PLUS12VPIN * 2));  // Clear the pull-up/pull-down register
  PLUS12VGPIO(PUPDR) |= (0x1u <<    (PLUS12VPIN * 2));   // Set push-pull to pull-up
  PLUS12VGPIO(ODR) ^= (-(unsigned long)PLUS12ONSTATE ^ PLUS12VGPIO(ODR)) & (1UL << PLUS12VPIN);


  PLUS5VGPIO(MODER) &= ~(0x3u <<   (PLUS5VPIN * 2));  // Clear mode register
  PLUS5VGPIO(MODER) |= (0x1u <<    (PLUS5VPIN * 2));   // Set IO direction to output mode
  PLUS5VGPIO(OSPEEDR) |= (0x3u <<  (PLUS5VPIN * 2)); // Set speed to high frequency
  PLUS5VGPIO(OTYPER) &= ~(0x1u <<  (PLUS5VPIN * 1)); // Set output to push-pull
  PLUS5VGPIO(PUPDR) &= ~(0x3u <<   (PLUS5VPIN * 2));  // Clear the pull-up/pull-down register
  PLUS5VGPIO(PUPDR) |= (0x1u <<    (PLUS5VPIN * 2));   // Set push-pull to pull-up
  PLUS5VGPIO(ODR) ^= (-(unsigned long)PLUS5ONSTATE ^ PLUS5VGPIO(ODR)) & (1UL << PLUS5VPIN);


  PLUS3V3GPIO(MODER) &= ~(0x3u <<   (PLUS3V3PIN * 2));  // Clear mode register
  PLUS3V3GPIO(MODER) |= (0x1u <<    (PLUS3V3PIN * 2));   // Set IO direction to output mode
  PLUS3V3GPIO(OSPEEDR) |= (0x3u <<  (PLUS3V3PIN * 2)); // Set speed to high frequency
  PLUS3V3GPIO(OTYPER) &= ~(0x1u <<  (PLUS3V3PIN * 1)); // Set output to push-pull
  PLUS3V3GPIO(PUPDR) &= ~(0x3u <<   (PLUS3V3PIN * 2));  // Clear the pull-up/pull-down register
  PLUS3V3GPIO(PUPDR) |= (0x1u <<    (PLUS3V3PIN * 2));   // Set push-pull to pull-up
  PLUS3V3GPIO(ODR) ^= (-(unsigned long)PLUS3V3ONSTATE ^ PLUS3V3GPIO(ODR)) & (1UL << PLUS3V3PIN);




  }

void BSP_SetLED(int Index) {
  if (Index == 0) {
    GPIOB_BSRR = (0x1u << LD1_PIN); // Turn LED on
  } else if (Index == 1) {
    GPIOB_BSRR = (0x1u << LD2_PIN); // Turn LED on
  } else if (Index == 2) {
    GPIOB_BSRR = (0x1u << LD3_PIN); // Turn LED on
  } else if (Index == 3) {
    GPIOG_BSRR = (0x1u << LaserPinR);
  }
}

/*********************************************************************
*
*       BSP_ClrLED()
*/
void BSP_ClrLED(int Index) {
  if (Index == 0) {
    GPIOB_BSRR = ((0x1u << 16) << LD1_PIN); // Turn LED off
  } else if (Index == 1) {
    GPIOB_BSRR = ((0x1u << 16) << LD2_PIN); // Turn LED off
  } else if (Index == 2) {
    GPIOB_BSRR = ((0x1u << 16) << LD3_PIN); // Turn LED off
  } else if (Index == 3) {
    GPIOG_BSRR = ((0x1u << 16) << LaserPinR);
  }
}

/*********************************************************************
*
*       BSP_ToggleLED()
*/
void BSP_ToggleLED(int Index) {
  if (Index == 0) {
    GPIOB_ODR ^= (0x1u << LD1_PIN); // Toggle LED
  } else if (Index == 1) {
    GPIOB_ODR ^= (0x1u << LD2_PIN); // Toggle LED
  } else if (Index == 2) {
    GPIOB_ODR ^= (0x1u << LD3_PIN); // Toggle LED
  } else if (Index == 3) {
    GPIOG_ODR ^= (0x1u << LaserPinR); // Toggle LED
  }
}

uint8_t getButtonValue() {

  return (GPIOC_IDR >> ButtonPin) & 1;
}

uint8_t getDACVALUE(){
uint32_t temp =(GPIOE_IDR >> DACSELECT) & 1;
   return temp;
}

uint8_t getButtons(){

return (GPIOA_IDR >> 5)&((1<<5)-1);
}


uint16_t getFunctionButtons(){

return GPIOF(IDR); 
}

uint16_t getKeyPadButtons(){

return GPIOE(IDR)>>2;
}





void writeLaserColor(uint8_t waarden) {

  GPIOG_ODR = (GPIOG_ODR & 0xFFF8) | waarden;
}
void writeIldaColor(uint8_t waarden) {

  uint8_t tempwaarden = 0;

  if (waarden == 0x38) {

    tempwaarden |= (1 << LaserPinR);
    tempwaarden |= (1 << LaserPinG);
    tempwaarden |= (1 << LaserPinB);

  } else if (waarden == 0x30) {
    tempwaarden |= (1 << LaserPinR);
    tempwaarden |= (1 << LaserPinB);

  } else if (waarden == 0x28) {
    tempwaarden |= (1 << LaserPinB);

  }

  else if (waarden == 0x18) {
    tempwaarden |= (1 << LaserPinG);
  } else if (waarden == 0x10) {
    tempwaarden |= (1 << LaserPinG);
    tempwaarden |= (1 << LaserPinR);
  }

  else if (waarden == 0x8) { //tekst
    //tempwaarden|=(1<<LaserPinG);
    tempwaarden |= (1 << LaserPinR);
  } else if (waarden == 0x0) { //lijn
    tempwaarden |= (1 << LaserPinR);
  }

  GPIOG_ODR = (GPIOG_ODR & 0xFFF8) | tempwaarden;
}