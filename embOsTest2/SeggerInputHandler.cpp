#include "SeggerInputHandler.h"

SeggerInputHandler::SeggerInputHandler() {

}
SeggerInputHandler::~SeggerInputHandler() {
}

void SeggerInputHandler::textInterperter(std::string input) {
  auto it = CommandStructs.begin();
   std::string commandString= input.substr(0,input.find_first_of(' '));
  while (it != CommandStructs.end()) {
   if(commandString.compare(it->commandName)==0){
   std::string argumetnString=input.substr(input.find_first_of(' ')+1,input.length());
      it->CommandFunction(argumetnString);
   }
    it++;
  }
}


/*
void SeggerInputHandler::helpFunction(void *restofInput) {
  printf("Dit is de help functie met argument %s\n\r", restofInput);
  auto it = CommandStructs.begin();
  while (it != CommandStructs.end()) {
    printf("------------------\n\rNaam : %20s \n\rFunctie omschrijving : %s\n\r------------------\n\r", it->commandName, it->Omschrijving);
    it++;
  }
}
*/
void SeggerInputHandler::addFunction(void (*functie)(std::string restofInput), std::string commandName) {

  addFunction(functie, commandName, "");
}

void SeggerInputHandler::addFunction(void (*functie)(std::string restofInput), std::string commandName, std::string help) {

  commandStruct Temp;
  Temp.Omschrijving = help;
  Temp.commandName = commandName;
  Temp.CommandFunction = functie;
 // Temp.CommandSize = getStringSize(Temp.commandName);
  CommandStructs.push_back(Temp);
}
void SeggerInputHandler::getInput() {
  char test;

  if (SEGGER_RTT_HasKey() == 1) {
    SEGGER_RTT_Read(0, &test, 1);
    if (test != '\n') {

      input.push_back(test);

    } else {
      if (inputLoc == 0) {
        printf("InputString : %s \n\r", input.c_str());
         textInterperter(input);
      } 
     
      input = "";
    }
  }
}
