
#pragma once
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

/*********************************************************************
*
*       General
*/
void BSP_Init      (void);
void BSP_SetLED    (int Index);
void BSP_ClrLED    (int Index);
void BSP_ToggleLED (int Index);
uint8_t getButtonValue ();

void writeLaserColor(uint8_t waarden);
void writeIldaColor(uint8_t waarden);
uint8_t getDACVALUE();
uint8_t getButtons();
uint16_t getKeyPadButtons();
uint16_t getFunctionButtons();



#ifdef __cplusplus
}
#endif