#ifdef __cplusplus
extern "C" {
#endif

 void MX_GPIO_Init(void);
 void MX_SDMMC1_SD_Init(void);
 void MX_SPI1_Init(void);
 void MX_TIM3_Init(void);
 void MX_TIM4_Init(void);
 void MPU_Config(void);
void SystemClock_Config(void);

#ifdef __cplusplus
}
#endif