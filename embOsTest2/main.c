/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 2014 - 2018 SEGGER Microcontroller GmbH             *
*                                                                    *
*           www.segger.com     Support: support@segger.com           *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* conditions are met:                                                *
*                                                                    *
* - Redistributions of source code must retain the above copyright   *
*   notice, this list of conditions and the following disclaimer.    *
*                                                                    *
* - Neither the name of SEGGER Microcontroller GmbH                  *
*   nor the names of its contributors may be used to endorse or      *
*   promote products derived from this software without specific     *
*   prior written permission.                                        *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED.                                                        *
* IN NO EVENT SHALL SEGGER Microcontroller GmbH BE LIABLE FOR        *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/
#include "main.h"
#include "HalDriverHandles.h"
#include "HalInitFunctions.h"
#include "NucleaoLeds.h"
#include "ProjectLaserFiles/DriverEnum.h"
#include "ProjectLaserFiles/Fat32FileSystem.h"
#include "ProjectLaserFiles/IldaPharser.h"
#include "ProjectLaserFiles/Laser.h"
#include "ProjectLaserFiles/SPIInterface.h"
#include "ProjectLaserFiles/SerialInterFaceDriver.h"
#include "ProjectLaserFiles/SerialInterfaceFac.h"

#include "RTOS.h"
#include "SEGGER_RTT.h"
#include "SeggerInputHandler.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

SD_HandleTypeDef hsd1;
SPI_HandleTypeDef hspi1;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

struct colorTableIlda {
  uint8_t rood;
  uint8_t groen;
  uint8_t blauw;
};
/*
colorTableIlda colortable[64] = {
    {255, 0, 0},
    {255, 16, 0},
    {255, 32, 0},
    {255, 48, 0},
    {255, 64, 0},
    {255, 80, 0},
    {255, 96, 0},
    {255, 112, 0},
    {255, 128, 0},
    {255, 144, 0},
    {255, 160, 0},
    {255, 176, 0},
    {255, 192, 0},
    {255, 208, 0},
    {255, 255, 0},
    {224, 255, 0},
    {192, 255, 0},
    {160, 255, 0},
    {128, 255, 0},
    {96, 255, 0},
    {64, 255, 0},
    {32, 255, 0},
    {0, 255, 0},
    {0, 255, 36},
    {0, 255, 73},
    {0, 255, 109},
    {0, 255, 146},
    {0, 255, 182},
    {0, 255, 219},
    {0, 255, 255},
    {0, 255, 255},
    {0, 227, 255},
    {0, 198, 255},
    {0, 170, 255},
    {0, 142, 255},
    {0, 113, 255},
    {0, 85, 255},
    {0, 56, 255},
    {0, 28, 255},
    {0, 0, 255},
    {32, 0, 255},
    {64, 0, 255},
    {96, 0, 255},
    {128, 0, 255},
    {160, 0, 255},
    {192, 0, 255},
    {224, 0, 255},
    {255, 0, 255},
    {255, 0, 255},
    {255, 32, 255},
    {255, 64, 255},
    {255, 96, 255},
    {255, 128, 255},
    {255, 160, 255},
    {255, 192, 255},
    {255, 224, 255},
    {255, 255, 255},
    {255, 224, 224},
    {255, 192, 192},
    {255, 160, 160},
    {255, 128, 128},
    {255, 96, 96},
    {255, 64, 64},
    {255, 32, 32}};
*/
//uint32_t ILDAFILE[(512 >> 2) * 19];
/*
uint16_t xwaardes[8192];
uint16_t ywaardes[8192];
uint16_t zwaardes[8192];
uint8_t LaserStatus[8192];
uint8_t LaserColors[8192];
*/
//uint32_t inputSD[512 >> 2];
uint8_t globalstate = 0;
uint32_t globalSDpage = 0;
uint32_t globalfilecount = 0;
SerialInterFaceDriver *globalSDinterface;
Fat32FileSystem *globalfilesystem;
PWMDriver *GlobalPWM;
SerialInterFaceDriver *SPIDriver;
Laser *globalLaser;
SeggerInputHandler *globalSeggerInput;
IldaPharser *globalIldaPharser;

Fat32FileSystem::FileInfo Filelist[30];

uint32_t globalbuffer[sizeof(LaserCoord) * 3000]; // 4 cluster buffer
//uint32_t globalprebuffer[8192 >> 2]; // 1 cluster buffer om een bestand tijdelijk te laden
//volatile extern uint32_t globalI = 0;
bool globalDemoEnable = false;
uint8_t globalSDReady = false;

struct testCord {

  uint32_t x;
  uint32_t y;
};

std::vector<LaserCoord (*)(uint32_t pos)> funcs;





std::vector<void (*)()> buttonFuncs;
std::vector<void (*)()> PointSpeedFuncs;
std::vector<void (*)()> FrameRateFuncs;
std::vector<void (*)()> PictureFuncs;


std::vector<std::vector<void(*)()>> allfuncs;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

static float sinus(float pos, uint16_t ampl, uint16_t offset);
static float cosinus(float pos, uint16_t ampl, uint16_t offset);

static void DebugInputNextImage(std::string input);
static void DebugInputPrevImage(std::string input);
static void DebugInputLedState(std::string input);
static void DebugInputNextSdPage(std::string input);
static void DebugInputPrevPage(std::string input);
static void DebugInputGoToPage(std::string input);
static void DebugEnableDemo(std::string input);
static void DebugDisableDemo(std::string input);
static void DebugtoggleDemo(std::string input);
static void DebugNewPicture(std::string input);
static void DebugPWM(std::string input);

static void DemoNewPicture(uint8_t getal);
void printSDPage(uint32_t page);
void SetSDDriver(SerialInterFaceDriver *interface);
uint32_t toHex(std::string input);

static LaserCoord DrawIlda(uint32_t pos);
static LaserCoord DrawCicrle(uint32_t pos);
static LaserCoord DrawSquare(uint32_t pos);

static LaserCoord DrawThing(uint32_t pos);
static LaserCoord DrawRocked(uint32_t pos);
static LaserCoord DrawSadle(uint32_t pos);
static LaserCoord DrawSadleRGB(uint32_t pos); //expirimental
static LaserCoord DrawIldaGoose(uint32_t pos);

static LaserCoord IldaDriverTest(uint32_t pos);
static LaserCoord IldaFileTest(uint32_t pos);
static LaserCoord TestLine(uint32_t pos); //expirimental
static LaserCoord Drawpoint(uint32_t pos);

unsigned char reverse(unsigned char b) {
  b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
  b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
  b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
  return b;
}

void PrintAnderTest(void *restofInput) {

  printf("dit is de 2de Test\n\r");
}

void DebugInputNextImage(std::string input) {
  globalstate++;
}
void DebugInputPrevImage(std::string input) {
  globalstate--;
}

void DebugInputLedState(std::string input) {

  printf("argument :%s\n\r", input.c_str());
}

void DebugInputNextSdPage(std::string input) {

  globalSDpage++;
  printSDPage(globalSDpage);
}

void DebugInputPrevPage(std::string input) {
  globalSDpage--;
  printSDPage(globalSDpage);
}

void DebugInputGoToPage(std::string input) {
  globalSDpage = toHex(input); // dont be a goof
  printSDPage(globalSDpage);
}
void DebugPWM(std::string input) {
  uint32_t temp = toHex(input);

  globalLaser->testPrescaler(temp & 0xFFFF);
}

void printSDPage(uint32_t page) {
  uint32_t SDPage[512 >> 2];
  (globalSDinterface->ReadData(page, SDPage, 1));
  printf("Block %2i (0x%08x)\n\r", page, page);
  printf("       0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F\n\r");
  for (int i = 0; i < 32; i++) {
    uint8_t *temp;
    temp = (uint8_t *)&SDPage[i * (16 >> 2)];
    printf("%02x0    ", i);
    // printf("\n0x%08x (0x%08x) 0x%08x ", SDPage[i], page, i << 2);
    for (int j = 0; j < 16; j++) {
      printf("%02X ", *temp);
      temp++;
    }
    temp = (uint8_t *)&SDPage[i * (16 >> 2)];
    for (int j = 0; j < 16; j++) {
      if (*temp >= 0x20 && *temp <= 0x7D) {
        printf("%c", *temp);
      } else {
        printf(" ");
      }
      temp++;
    }
    printf("\n\r");
  }
}

void DebugEnableDemo(std::string input) {
  globalDemoEnable = true;
}
void DebugDisableDemo(std::string input) {
  globalDemoEnable = false;
}
void DebugtoggleDemo(std::string input) {
  globalDemoEnable ^= 1;
}

bool globalRead = false;
bool globalFilled = false;
uint8_t globalFileReaderpos = 0;
void DebugNewPicture(std::string input) {

  uint8_t getal = toHex(input);
  globalFileReaderpos = getal;
  globalRead = false;
  globalFilled = false;
}
void DemoNewPicture(uint8_t getal) {

  globalFileReaderpos = getal%globalfilecount;
  globalRead = false;
  globalFilled = false;
}

void SetSDDriver(SerialInterFaceDriver *interface) {
  globalSDinterface = interface;
}
uint32_t toHex(std::string input) {
  uint32_t returnnumb = 0;
  for (uint32_t i = 0; i < input.length(); i++) {
    if (input[i] >= '0' && input[i] <= '9') {
      returnnumb = ((returnnumb << 4) | (input[i] - '0'));
    } else if (input[i] >= 'A' && input[i] <= 'F') {
      returnnumb = ((returnnumb << 4) | (input[i] - 0x37));
    }
  }
  return returnnumb;
}

static float sinus(float pos, uint16_t ampl, uint16_t offset) {

  return (((sin(pos) + 1.0) / 2) * ampl) + offset;
}
float cosinus(float pos, uint16_t ampl, uint16_t offset) {
  return (((cos(pos) + 1.0) / 2) * ampl) + offset;
}
/*
LaserCoord DrawIlda(uint32_t pos) {
  uint32_t tempopos = pos % 1191;
  LaserCoord tempCord;
  tempCord.Pos.x = (xwaardes[tempopos] ^ 0x8000); //toggle eerste bit om er een signed getal er van te maken
  tempCord.Pos.y = (ywaardes[tempopos] ^ 0x8000);
  if ((LaserStatus[tempopos] >> 6) & 1 == 1) {

    writeLaserColor(0);
  } else {

    writeIldaColor(LaserColors[tempopos]);
  }

  return tempCord;
}

LaserCoord DrawIldaGoose(uint32_t pos) {
  uint32_t tempopos = pos % 400;
  LaserCoord tempCord;
  tempCord.Pos.x = (xwaardes[tempopos] ^ 0x8000); //toggle eerste bit om er een signed getal er van te maken
  tempCord.Pos.y = (ywaardes[tempopos] ^ 0x8000);
  if ((LaserStatus[tempopos] >> 6) & 1 == 1) {

    writeLaserColor(0);
  } else {

    writeLaserColor(3);
  }

  return tempCord;
}
*/
LaserCoord TestLine(uint32_t pos) {
  LaserCoord tempCord;

  static uint8_t rood = 20;
  static int8_t roodir = 10;
  static uint8_t groen = 20;
  static int8_t groendir = 0;
  static uint8_t blauw = 20;
  static int8_t blauwdir = 0;

  if (pos % 1200 == 0) {
    if (blauw >= 230) {
      blauwdir = -10;
      groendir = 10;
      roodir = 0;
    }
    if (groen >= 230) {
      roodir = 10;
      groendir = -10;
      blauwdir = 0;
    }
    if (rood >= 230) {
      roodir = -10;
      blauwdir = 10;
      groendir = 0;
    }
    rood += roodir;
    groen += groendir;
    blauw += blauwdir;
  }
  static float pos2;
  float points = 1200.0;

  pos2 = ((fmod((float)pos, points)) / points) * (3.14 * 2);

  tempCord.Pos.x = (uint32_t)(((sin(pos2) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.y = pos; // (uint32_t)(((sin(pos2) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.z = 0;
  tempCord.Color.green = groen;
  tempCord.Color.red = rood;
  tempCord.Color.blue = blauw;
  return tempCord;
}
LaserCoord DrawCirle(uint32_t pos) {

  LaserCoord tempCord;
  static uint8_t rood = 20;
  static int8_t roodir = 10;
  static uint8_t groen = 20;
  static int8_t groendir = 0;
  static uint8_t blauw = 20;
  static int8_t blauwdir = 0;

  if (pos % 10 == 0) {
    if (blauw >= 230) {
      blauwdir = -10;
      groendir = 10;
      roodir = 0;
    }
    if (groen >= 230) {
      roodir = 10;
      groendir = -10;
      blauwdir = 0;
    }
    if (rood >= 230) {
      roodir = -10;
      blauwdir = 10;
      groendir = 0;
    }
    rood += roodir;
    groen += groendir;
    blauw += blauwdir;
  }
  static float pos2 = 0; //fuck dit
  static float pos3 = 0;
  float pointsincircle = 1200.0;

  pos2 = ((fmod((float)pos, pointsincircle)) / pointsincircle) * (3.14 * 2);

  tempCord.Pos.x = (uint32_t)(((sin(pos2) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.y = (uint32_t)(((cos(pos2) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.z = 0;
  tempCord.Color.green = groen;
  tempCord.Color.red = rood;
  tempCord.Color.blue = blauw;

  return tempCord;
}

LaserCoord DrawSadleRGB(uint32_t pos) {
  LaserCoord tempCord;
  static uint8_t rood = 20;
  static int8_t roodir = 10;
  static uint8_t groen = 20;
  static int8_t groendir = 0;
  static uint8_t blauw = 20;
  static int8_t blauwdir = 0;
  if (pos % 15 == 0) {
    if (blauw >= 230) {
      blauwdir = -10;
      groendir = 10;
      roodir = 0;
    }
    if (groen >= 230) {
      roodir = 10;
      groendir = -10;
      blauwdir = 0;
    }
    if (rood >= 230) {
      roodir = -10;
      blauwdir = 10;
      groendir = 0;
    }
    rood += roodir;
    groen += groendir;
    blauw += blauwdir;
  }
  static float pos3 = 0;
  static float pos4 = 0;
  float points1 = 500.0 * 2.0;
  float points2 = 1050.0 * 2.0;
  pos3 = ((fmod((float)pos, points1)) / points1) * (3.14 * 2);

  pos4 = ((fmod((float)pos, points2)) / points2) * (3.14 * 2);
  /*  pos3 = pos3 + ((1.0 / 30.0) * 3.14);
  pos4 = pos4 + ((1.0 / 60.0) * 3.14);
  if (pos3 > 3.14 * 2.0) {
    pos3 = 0.0;
  }
  if (pos4 > 3.14 * 4.0) {
    pos4 = 0.0;
  }
  */
  tempCord.Color.red = rood;
  tempCord.Color.blue = blauw;
  tempCord.Color.green = groen;
  tempCord.Pos.x = (uint32_t)(((sin(pos4) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.y = (uint32_t)(((cos(pos3) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.z = 0;
  return tempCord;
}

LaserCoord DrawThing(uint32_t pos) {
  writeLaserColor(4);
  static float pos2 = 0; //fuck dit
  static float pos3 = 0;
  LaserCoord tempCord;
  pos2 = pos2 + ((1.0 / 60.0) * 3.14);
  pos3 = pos3 + ((1.0 / 30.001) * 3.14);
  if (pos2 > 3.14 * 2.0) {
    pos2 = 0.0;
  }

  if (pos3 > 3.14 * 2.0) {
    pos3 = 0.0;
  }

  tempCord.Pos.x = (uint32_t)(((sin(pos2) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.y = (uint32_t)(((cos(pos3) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.z = 0;

  return tempCord;
}
LaserCoord DrawRocked(uint32_t pos) {
  uint16_t circleRadius = 0xFFFF >> 2;
  uint32_t tempos = pos % (3000 / 2);
  writeLaserColor((tempos % 120000) & 0x7);
  uint32_t xpos = (0xFFFF >> 1) - (0xFFFF >> 2);
  static float pos2 = 0; //fuck dit
  static float pos3 = 0;
  static float pos4 = 0;
  LaserCoord tempCord;
  pos2 = pos2 + ((1.0 / 60.0) * 3.14);

  if (pos2 > 3.14 * 2.0) {
    pos2 = 0.0;
  }

  if (tempos < (1000 / 2)) {
    tempCord.Pos.x = sinus(pos2, circleRadius, xpos);
    tempCord.Pos.y = cosinus(pos2, circleRadius, 0);
  } else if (tempos >= (1000 / 2) && tempos < (2000 / 2)) {
    tempCord.Pos.x = sinus(pos2, circleRadius, xpos + circleRadius);
    tempCord.Pos.y = cosinus(pos2, circleRadius, 0);

  } else {
    pos3 = pos3 + ((1.0 / 60.0) * 3.14);
    pos4 = pos4 + ((1.0 / 60.0) * 3.14);

    if (pos3 > 3.14 * 2.0) {
      pos3 = 0.0;
    }
    if (pos4 > 3.14 * 1.0) {
      pos4 = 0.0;
    }

    tempCord.Pos.x = (uint32_t)(((cos(pos3) + 1.0) / 3) * (0xFFFF >> 1) + xpos + (xpos / 3));
    tempCord.Pos.y = (uint32_t)(((sin(pos4) + 0.5) / 1) * (0xFFFF >> 1));
  }
  tempCord.Pos.y ^= 0xFFFF; //flip image
  tempCord.Pos.z = 0;
  tempCord.Color.red = 244;
  tempCord.Color.blue = 66;
  tempCord.Color.green = 95;
  return tempCord;
}
LaserCoord DrawSadle(uint32_t pos) {
  LaserCoord tempCord;
  static float pos3 = 0;
  static float pos4 = 0;

  pos3 = pos3 + ((1.0 / 30.0) * 3.14);
  pos4 = pos4 + ((1.0 / 60.0) * 3.14);
  if (pos3 > 3.14 * 2.0) {
    pos3 = 0.0;
  }
  if (pos4 > 3.14 * 4.0) {
    pos4 = 0.0;
  }
  tempCord.Color.red = 0;
  tempCord.Color.blue = 255;
  tempCord.Color.green = 255;
  tempCord.Pos.x = (uint32_t)(((sin(pos4) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.y = (uint32_t)(((cos(pos3) + 1.0) / 2) * 0xFFFF);
  tempCord.Pos.z = 0;
  return tempCord;
}
LaserCoord Drawpoint(uint32_t pos) {
  LaserCoord tempCord;

  tempCord.Color.red = 127;
  tempCord.Color.blue = 127;
  tempCord.Color.green = 127;
  tempCord.Pos.x = 0xFFFF >> 1;
  tempCord.Pos.y = 0xFFFF >> 1;
  tempCord.Pos.z = 0;
  return tempCord;
}

LaserCoord DrawSquare(uint32_t pos) {
  uint8_t state = (pos & 0x300) >> 8;
  LaserCoord tempCord;
  tempCord.Pos.x = 0;
  tempCord.Pos.y = 0;
  LaserCoord middle;
  middle.Pos.x = UINT16_MAX >> 1;
  middle.Pos.y = UINT16_MAX >> 1;
  uint16_t size = 0xFFFF >> 1;
  if (state == 0) {
    tempCord.Pos.x = middle.Pos.x + size;
    tempCord.Pos.y = middle.Pos.y + size;
  } else if (state == 1) {
    tempCord.Pos.x = middle.Pos.x - size;
    tempCord.Pos.y = middle.Pos.y + size;
  } else if (state == 2) {
    tempCord.Pos.x = middle.Pos.x - size;
    tempCord.Pos.y = middle.Pos.y - size;

  } else if (state == 3) {
    tempCord.Pos.x = middle.Pos.x + size;
    tempCord.Pos.y = middle.Pos.y - size;
  }
  tempCord.Pos.z = 0;
  tempCord.Color.red = 127;
  tempCord.Color.green = 127;
  tempCord.Color.blue = 127;
  return tempCord;
}

/*
LaserCoord IldaFileTest(uint32_t pos) { // alleen format 0 word ondersteunt met 1 frame kleiner dan 8192 bytes
  LaserCoord tempCord;
  static uint32_t count = 0;
  static uint32_t readerpos = 0;
  static uint32_t pointreader = 0;

  static uint32_t recordcount = 0;
  static uint32_t frame = 0;
  static bool nextframewished = false;
  static uint32_t ReadPosInBuffer = 0;
  static IldaHeader Header;
  if (!globalRead) {
    count = 0;

    globalfilesystem->StreamFile(Filelist[globalFileReaderpos].name, globalbuffer, 0, Filelist[globalFileReaderpos].sizeInBytes % (8192 * 16)); // lees buffer
   // Header = globalLaser->ilda->getIldaHeader(globalbuffer);
   // uint8_t *readerpos = (uint8_t *)&globalbuffer;
   // readerpos += 32;
   // globalLaser->ilda->parseStreamData(readerpos, LaserCordBuffer, Header, Header.NumberOfRecords * 8);
    globalRead = true;
    frame = 0;
    //readerpos-=32;

    recordcount = (((globalbuffer[6] & 0xFF) << 8) | ((globalbuffer[6] >> 8) & 0xFF)) & 0xFFFF;
   
    count = recordcount;
    ReadPosInBuffer = 0;
  }
  if (pos % 1000 == 0) {

    nextframewished = true;
  }

  if (readerpos == recordcount - 1 && nextframewished && (globalFileReaderpos == 0 || globalFileReaderpos == 2 || globalFileReaderpos == 6 || globalFileReaderpos == 3)) {

    globalFilled = false;

    recordcount = (((globalbuffer[ReadPosInBuffer + 6] & 0xFF) << 8) | ((globalbuffer[ReadPosInBuffer + 6] >> 8) & 0xFF)) & 0xFFFF;
    ReadPosInBuffer += (((recordcount) << 1) + 8); //recordcount *2 + eerste ilda header + 6 als offset voor de aantal records
    frame++;
    if (frame == 19 && globalFileReaderpos == 0) { //19
      frame = 0;
      ReadPosInBuffer = 0;
    }
    if (frame == 25 && (globalFileReaderpos == 2)) {
      frame = 0;
      ReadPosInBuffer = 0;
    }
    if (frame == 18 && (globalFileReaderpos == 6)) {
      frame = 0;
      ReadPosInBuffer = 0;
    }
    if (frame == 10 && (globalFileReaderpos == 3)) {
      frame = 0;
      ReadPosInBuffer = 0;
    }

    nextframewished = false;
    readerpos = 0;
  }
  if (!globalFilled) {
    for (int i = 0; i < recordcount; i++) { //laser format 0
      xwaardes[i] = 0;
      ywaardes[i] = 0;
      LaserStatus[i] = 0;
      LaserColors[i] = 0;
      xwaardes[i] =
          xwaardes[i] = (globalbuffer[((i) << 1) + 0x8 + ReadPosInBuffer] & 0xFFFF);
      xwaardes[i] = ((xwaardes[i] >> 8) | (xwaardes[i] << 8)) & 0xFFFF;
      ywaardes[i] = (globalbuffer[((i) << 1) + 0x8 + ReadPosInBuffer] >> 16) & 0xFFFF;
      ywaardes[i] = ((ywaardes[i] >> 8) | (ywaardes[i] << 8)) & 0xFFFF;
      LaserStatus[i] = (globalbuffer[((i) << 1) + 0x9 + ReadPosInBuffer] >> 16) & 0xFF;
      LaserColors[i] = ((globalbuffer[((i) << 1) + 0x9 + ReadPosInBuffer] >> 24) & 0xFF);
    }
    globalFilled = true;
  }

  tempCord.Pos.x = (xwaardes[readerpos] ^ 0x8000); //toggle eerste bit om er een signed getal er van te maken
  tempCord.Pos.y = (ywaardes[readerpos] ^ 0x8000);
  uint8_t LaserEnable = ((LaserStatus[readerpos] >> 6) & 1) ^ 1; // toggle omdat een 1 een laserdisable is
  tempCord.Color.green = colortable[LaserColors[readerpos]].groen * LaserEnable;
  tempCord.Color.red = colortable[LaserColors[readerpos]].rood * LaserEnable;
  tempCord.Color.blue = colortable[LaserColors[readerpos]].blauw * LaserEnable;

  if (readerpos >= recordcount - 1) {
    readerpos = 0;
    tempCord.Color.green = 0;
    tempCord.Color.red = 0;
    tempCord.Color.blue = 0;

  } else {
    readerpos++;
  }

  return tempCord;
}
*/

static OS_STACKPTR int LaserStack[8192>>1];
static OS_STACKPTR int CommandStack[100]; // Task stacks
static OS_TASK TaskCommand;
static OS_TASK TaskLaser;
static OS_TASK TaskFile;
static OS_TASK TaskSdTester;
static OS_STACKPTR uint32_t StackSDTester[4]; //8192*4 bytes
static OS_STACKPTR int FileSystemStack[8192>>1];         //8192*4 bytes
uint32_t OldI = 0;
static OS_TASKEVENT timerEvent;

typedef struct __attribute__((packed)) SDRequestType {
  uint32_t addres;
  uint32_t count;
  uint32_t *Databuffer;
};

typedef struct __attribute__((packed)) fileRequest {
  std::string Filename;
  uint32_t *buffer;
  uint32_t startpoint;
  uint32_t size;
  OS_MAILBOX *Done;
};

#define MAX_SD_Queueae 32

static OS_QUEUE SDinQueue;
static fileRequest SDQueue[MAX_SD_Queueae];

LaserCoord LaserCordBuffer[4096];
LaserCoord LaserCordBuffer2[4096];

enum newFrameStates { NORMAL,
  READHEADER,
  HEADERREAD,
  FILEREAD,
  PARSEFRAME };

static OS_MAILBOX SDReady;

uint32_t globalFrameRate=1;
uint32_t globalPointRate=12000;


LaserCoord IldaDriverTest(uint32_t pos) {
  LaserCoord tempCord;
  static uint32_t count = 0;
  static uint32_t readerpos = 0;
  static uint32_t pointreader = 0;

  static uint32_t recordcount = 0;
  static uint32_t frame = 0;

  static bool nextframewished = false;
  static bool nextframeReadRequest = false;

  static uint32_t currentposinfile = 0;

  static newFrameStates FrameState = PARSEFRAME;
  static char SDReadDone = 0;
  static fileRequest test;
  static fileRequest tes2;
  static IldaHeader Header;

  OS_MAILBOX_Get1(&SDReady, &SDReadDone);

  if (!globalRead) {
    count = 0;
    uint32_t done = 0;
    fileRequest test;
    test.Filename = Filelist[globalFileReaderpos].name;
    test.buffer = globalbuffer;
    test.size = 32;
    test.startpoint = 0;
    test.Done = &SDReady;

    while (OS_QUEUE_Put(&SDinQueue, &test, sizeof(fileRequest)) != 0) {
      OS_TASK_Delay(1);
    }
    while (!SDReadDone) {
      OS_TASK_Delay(1);
      OS_MAILBOX_Get1(&SDReady, &SDReadDone);
    }
    SDReadDone = 0;
    Header = globalLaser->ilda->getIldaHeader(globalbuffer);
    uint32_t bytesperrecord = globalLaser->ilda->getBytesPerRecord(Header);

    globalfilesystem->StreamFile(Filelist[globalFileReaderpos].name, globalbuffer, 32, Header.NumberOfRecords * bytesperrecord);
    uint8_t *readerposbuffer = (uint8_t *)&globalbuffer;

    globalLaser->ilda->parseStreamData(readerposbuffer, LaserCordBuffer, Header, Header.NumberOfRecords);

    currentposinfile = (bytesperrecord * Header.NumberOfRecords) + 32;
    globalRead = true;

    recordcount = Header.NumberOfRecords;
    count = recordcount;

    readerpos = 0;
    FrameState=PARSEFRAME;
  }

  if (FrameState == READHEADER) {
    if (SDReadDone == 1) {
      SDReadDone = 0;
      FrameState = HEADERREAD;
    }
  }
  if (FrameState == HEADERREAD) {
    Header = globalLaser->ilda->getIldaHeader(globalbuffer);
   
    while (Header.ILDA[0] != 0x49 and Header.ILDA[1] != 0x4C and Header.ILDA[2] != 0x44 and Header.ILDA[3] != 0x41) {
      int a = 100;
  
      a++;
 
    
    }
     uint32_t bytesperrecord = globalLaser->ilda->getBytesPerRecord(Header);
    if (Header.NumberOfRecords == 0) {
      currentposinfile = 0;
      FrameState = PARSEFRAME;

    } else {
      test.Filename = Filelist[globalFileReaderpos].name;
      test.buffer = globalbuffer;
      test.size = Header.NumberOfRecords * bytesperrecord;
      test.startpoint = currentposinfile;
      test.Done = &SDReady;

      while (OS_QUEUE_Put(&SDinQueue, &test, sizeof(fileRequest)) != 0) {
       // OS_TASK_Delay(1);
      }
      FrameState = FILEREAD;
    }
  }
  if (FrameState == FILEREAD) {
    if (SDReadDone == 1) {
      SDReadDone = 0;
      FrameState = PARSEFRAME;
      uint8_t *readerposbuffer = (uint8_t *)&globalbuffer;
      globalLaser->ilda->parseStreamData(readerposbuffer, LaserCordBuffer, Header, Header.NumberOfRecords);
      uint32_t bytesperrecord = globalLaser->ilda->getBytesPerRecord(Header);
      recordcount = Header.NumberOfRecords;
      currentposinfile += (bytesperrecord * Header.NumberOfRecords);
      if (Header.FrameNumber == Header.totalFrames) {
        currentposinfile = 0;
        frame = 0;
      }
    }
  }

  tempCord = LaserCordBuffer[readerpos];
  if(globalFrameRate!=0){
  if (pos % (globalPointRate/globalFrameRate) == (globalPointRate/globalFrameRate)/2){//) { //12000/10 = 10 FPS

    nextframewished = true;
  }
  if ((pos % ((globalPointRate/globalFrameRate)/2)) == 0 and FrameState == PARSEFRAME) { // vraag de nieuwe header
    nextframeReadRequest = true;
    frame++;

    tes2.Filename = Filelist[globalFileReaderpos].name;
    tes2.buffer = globalbuffer;
    tes2.size = 32;
    tes2.startpoint = currentposinfile;
    tes2.Done = &SDReady;

    while (OS_QUEUE_Put(&SDinQueue, &tes2, sizeof(fileRequest)) != 0) {
     // OS_TASK_Delay(1);
    }
    currentposinfile += 32;
    FrameState = READHEADER;
  }
}
  if (readerpos >= recordcount - 1) {
    readerpos = 0;
    tempCord.Color.green = 0;
    tempCord.Color.red = 0;
    tempCord.Color.blue = 0;

  } else {
    readerpos++;
  }

  return tempCord;
}

static void FileSystemTask(void) { // Task wat een SD kaart uit leest

  // globalSDinterface->ReadData()
  int j = 0;
  j++;
  int requestsize;
  char donesignal = 1;
  fileRequest *tempbuffer;
  while (1) {
    requestsize = OS_QUEUE_GetPtrBlocked(&SDinQueue, (void **)&tempbuffer);
    globalfilesystem->StreamFile(tempbuffer->Filename, tempbuffer->buffer, tempbuffer->startpoint, tempbuffer->size);
    OS_QUEUE_Purge(&SDinQueue);
    OS_MAILBOX_Put1(tempbuffer->Done, &donesignal);
  }
}
uint8_t pichanged = 0;
static void LaserTask(void) {
  uint32_t pos = 0;

  LaserCoord tempcord;
  uint8_t picDemoReaderpos = 0;

  while (1) {
    timerEvent = OS_TASKEVENT_GetBlocked(0xFF);

    pos++;
   BSP_ToggleLED(1);
//BSP_ToggleLED(2);
    if (pos % 60000 == 0 && globalDemoEnable) { // 5 seconden per plaatje
      globalstate++;
      if (globalstate % funcs.size() == 0) {
    //    DemoNewPicture(picdemos[(picDemoReaderpos++) % 6]);
      }
    }
    if (globalstate % funcs.size() == 0 and pichanged == 0) {
      //  globalFileReaderpos++;
      pichanged = 1;
      if (globalFileReaderpos == globalfilecount) {
        globalfilecount = 0;
        globalFileReaderpos = 0;
        // globalstate++;
      }
    }

    tempcord = funcs.at(globalstate % funcs.size())(pos);
    // tempcord.Pos.x ^= 0xFFFF;
    globalLaser->testpoint2(&tempcord);
  }
}

 static float rotx = 180.0;
  static float roty = 0.0;
  static float rotz = 0.0;


static void Left(void) {

  roty-=0.1f;
}
static void Right(void) {
  roty+=0.1f;
}
static void Up(void) {
   rotx+=0.1f;
}
static void Down(void) {
   rotx-=0.1f;
}
static void Mid(void) {
  rotx=180.0;
  roty=0;
  rotz=0;
}

static float pixelfreq =30.0;


static void updateSpeed(float value){
float period = (1/value)*1000;
globalPointRate= (uint32_t)value*1000;
uint16_t tempvalue =(uint16_t) period;
 

  (*(volatile unsigned int *)(0x4000042C)) =tempvalue;

}

static void PixelSpeedUp(void) {

  pixelfreq+= 0.5f;

if(pixelfreq >=30.0f){
pixelfreq = 30.0f;

}
  updateSpeed(pixelfreq);




}
static void PixelSpeedDown(void) {
  

  pixelfreq-= 0.5f;
 if(pixelfreq <=0.0f){
pixelfreq = 0.1f;
}

  updateSpeed(pixelfreq);
 
}
static void PixelSpeedDummy0(void) {
  
}
static void PixelSpeedDummy1(void) {
  
}
static void PixelSpeedDefault(void) {
  pixelfreq= 12.0f;
  updateSpeed(pixelfreq);
 
}



static void FrameSpeedUp(void) {
globalFrameRate++;
if(globalFrameRate >=30){
globalFrameRate=30;
}

}
static void FrameSpeedDown(void) {
globalFrameRate--;
if(globalFrameRate <=0){
globalFrameRate=0;
}
            
}           
static void FrameSpeedDummy0(void) {
           
}          
static void FrameSpeedDummy1(void) {
           
}          
static void FrameSpeedDefault(void) {
globalFrameRate=15;
 
}

static void nextpic(void) {




  DemoNewPicture(globalFileReaderpos +1 );

}
static void prevpic(void) {
   DemoNewPicture(globalFileReaderpos-1 );
}
static void nexttest(void) {
 //  rotx+=0.01f;
}
static void prevtest(void) {
  // rotx-=0.01f;
}
static void defaultpic(void) {
  DemoNewPicture(0);
}





uint8_t totalpics=0;






static void CommandTask(void) {
  uint8_t buttest = 0;
  uint8_t prevbut = 0;
  uint8_t commandState=0;
  uint8_t buttons = 0;
  printf("Debug command handler ready\n\r");
  while (1) {
    globalSeggerInput->getInput();
    buttest = getButtonValue();
   
    globalLaser->setRotation(rotx, roty, rotz);
  // globalLaser->setRotation(0, 0, 0);
    buttons = getKeyPadButtons();
    commandState = getFunctionButtons();


   buttons ^= 0x1F;
    for (int i = 0; i < 5; i++) {
     int value =(buttons>>i)&1;
      if (value) {
        allfuncs.at(commandState&0x3).at(i)();
      }
    }

    if (buttest != prevbut) {
      globalRead = false;
      globalFilled = false;
      prevbut = buttest;
      
   //   globalstate += prevbut;
      globalFileReaderpos += prevbut;
      globalFileReaderpos =globalFileReaderpos %globalfilecount;
      pichanged = 0;
    }
    OS_TASK_Delay(5000);
  }
}
uint32_t databuffer[512 >> 1];
static void SDTesterTask(void) {

  SDRequestType test = {0, 1, databuffer};
  while (1) {

    // while (OS_QUEUE_Put(&SDinQueue, &test, sizeof(SDRequestType)) != 0) {
    //    OS_TASK_Delay(1);
    // }

    OS_TASK_Delay(1);

    test.addres++;
  }
}

void TimerInteruptHandler() {
  //  static uint8_t i = 0;
  OS_TASKEVENT_Set(&TaskLaser, 0xFF);
  //i++;
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi){

 // BSP_ToggleLED(0);
 SPIInterface::Own.SPIIRQHandler();
}

void SpiHandler(){
auto *hspi=&hspi1;
  uint32_t itsource = hspi->Instance->IER;
  uint32_t itflag   = hspi->Instance->SR;
  uint32_t trigger  = itsource & itflag;
// if (HAL_IS_BIT_SET(trigger, SPI_FLAG_EOT)){

 
//}
}



char SDrequestbuffer[10];
//uint32_t DMATEST[8192 << 2];
int main(void) {

  HAL_Init();

  SystemClock_Config();
  OS_Init();   // OS init na de clock config en hal init
  OS_InitHW(); // HAL init veranderd de klok van de Hal Driver
               // OS moet voor omdat die geinit moet worden omdat de HAL_Delay de OS_delay aanroept
  MX_GPIO_Init();
  MX_SDMMC1_SD_Init();
  MX_SPI1_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  SEGGER_RTT_Init();
  BSP_Init();
  FrameSpeedDefault();

  SPIANDGPIOstruct structtester;
  structtester.SpiHandle = &hspi1;
  LaserConfigs WantedLaser;
  if (getDACVALUE() == 1) {
    WantedLaser.Dac = SPIDACC;
  } else {
    WantedLaser.Dac = SPIDACC;
  }
  //

  WantedLaser.PWM = INTERNALPWM;
  WantedLaser.laser = LaserGreen;
  SlaveSelectStruct slavex;
  SlaveSelectStruct slavey;

  if (WantedLaser.Dac == I2CDACC) {
    slavex = {GPIOG, GPIO_PIN_1, 0};
    slavey = {GPIOF, GPIO_PIN_9, 0};

  } else {
    slavex = {GPIOF, GPIO_PIN_10, 0};
    slavey = {GPIOF, GPIO_PIN_11, 0};
  }

  WantedLaser.HalDriver = (void *)&structtester;
  structtester.SlaceSelectArray[1] = slavey;
  structtester.SlaceSelectArray[0] = slavex;

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = slavex.GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

  HAL_GPIO_Init(slavex.GPIOHanndle, &GPIO_InitStruct);
  GPIO_InitStruct.Pin = slavey.GPIO_Pin;
  HAL_GPIO_Init(slavey.GPIOHanndle, &GPIO_InitStruct);

  OS_TASK_CREATE(&TaskLaser, "Laser Task", 100, LaserTask, LaserStack);
  OS_TASK_CREATE(&TaskCommand, "Command Task", 150, CommandTask, CommandStack);
  OS_MAILBOX_Create(&SDReady, 1, 8, &SDrequestbuffer);

  if (globalSDReady) {

    OS_TASK_CREATE(&TaskFile, "SD task", 199, FileSystemTask, FileSystemStack);
    OS_QUEUE_Create(&SDinQueue, &SDQueue, sizeof(SDQueue));

    SerialInterFaceDriver *SDDriver = SerialInterfaceFac::makeInterface(SDIO);
    SDDriver->addHalDriver((void *)&hsd1);
    Fat32FileSystem *FatTest = new Fat32FileSystem(SDDriver);
    globalfilesystem = FatTest;
    SetSDDriver(SDDriver);
    funcs.push_back(IldaDriverTest);
    // funcs.push_back(IldaFileTest);

    FatTest->listAllFilesWithExtension("ILD", Filelist, 100);
    for (int i = 0; i < 30; i++) {
      if (Filelist[i].sizeInBytes != 0) {
        printf("Naam : %15s , Groote : %10i   I = %X\n\r", Filelist[i].name.c_str(), Filelist[i].sizeInBytes, i);
        globalfilecount++;
        for (int j = 0; j < 100; j++) {
          asm("NOP");
        }
      }
    }
  }
  globalLaser = new Laser(WantedLaser, globalfilesystem);
  if (globalSDReady) {
    // globalLaser->selectIlda(Filelist[0].name);
  }

  globalSeggerInput = new SeggerInputHandler();

  globalSeggerInput->addFunction(DebugInputNextImage, "next", "test2");
  globalSeggerInput->addFunction(DebugInputPrevImage, "prev", "schrijf via SPI naar de DAC");
  globalSeggerInput->addFunction(DebugInputLedState, "Led", "schrijf via SPI naar de DAC");
  globalSeggerInput->addFunction(DebugInputPrevPage, "ppage", "schrijf via SPI naar de DAC");
  globalSeggerInput->addFunction(DebugInputNextSdPage, "npage", "schrijf via SPI naar de DAC");
  globalSeggerInput->addFunction(DebugInputGoToPage, "page", "schrijf via SPI naar de DAC");
  globalSeggerInput->addFunction(DebugEnableDemo, "enable", "");
  globalSeggerInput->addFunction(DebugDisableDemo, "disable", "");
  globalSeggerInput->addFunction(DebugtoggleDemo, "demo", "");
  globalSeggerInput->addFunction(DebugNewPicture, "pic", "");
  globalSeggerInput->addFunction(DebugPWM, "PWM", "schrijf via SPI naar de DAC");

  //funcs.push_back(DrawSadleRGB);
 // funcs.push_back(TestLine);
  //funcs.push_back(DrawSquare);
  funcs.push_back(DrawCirle);
  funcs.push_back(DrawSadle);
  //funcs.push_back(DrawThing);
  // funcs.push_back(DrawRocked);


 buttonFuncs.push_back(Right);
 buttonFuncs.push_back(Up);
   buttonFuncs.push_back(Down);
    buttonFuncs.push_back(Mid);
 
   buttonFuncs.push_back(Left);
  

 


  PointSpeedFuncs.push_back(PixelSpeedDummy0);
  PointSpeedFuncs.push_back(PixelSpeedUp);
  PointSpeedFuncs.push_back(PixelSpeedDown);
  PointSpeedFuncs.push_back(PixelSpeedDefault);
  PointSpeedFuncs.push_back(PixelSpeedDummy1);
  
  FrameRateFuncs.push_back(FrameSpeedDummy0);
  FrameRateFuncs.push_back(FrameSpeedUp);
  FrameRateFuncs.push_back(FrameSpeedDown);
  FrameRateFuncs.push_back(FrameSpeedDefault);

  FrameRateFuncs.push_back(FrameSpeedDummy1);

  PictureFuncs.push_back(nextpic);
  PictureFuncs.push_back(nextpic);
  PictureFuncs.push_back(prevpic);
  PictureFuncs.push_back(defaultpic);
  
  PictureFuncs.push_back(prevpic);

 
 
   allfuncs.push_back(PointSpeedFuncs);
 allfuncs.push_back(buttonFuncs);

  allfuncs.push_back(FrameRateFuncs);
   allfuncs.push_back(PictureFuncs);


 

  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
  HAL_TIM_Base_Start_IT(&htim3);

  OS_Start();
  while (1) {
  }

  return 0;
}