#include "SPIInterface.h"
#include "SEGGER_RTT.h"
#include "stdint.h"

SPIInterface SPIInterface::Own;
SPIInterface::SPIInterface() : SerialInterFaceDriver(SPI) {

  // geen init nodig omdat de HAL drivers gebruikt worden
  // diir een
}

void SPIInterface::SPIIRQHandler() { //TODO fix dit
  static uint8_t interruptcounter = 0;

  IRQBuffertype prev = Buffer[(Readpos)&BUFFERSIZE];

  deselectSlave(prev.SpiandSlave->SlaceSelectArray[prev.addres]);


   Readpos++;
    IRQBuffertype temp = Buffer[Readpos & BUFFERSIZE];
    selectSlave(temp.SpiandSlave->SlaceSelectArray[temp.addres]);
     while (temp.SpiandSlave->SpiHandle->State != HAL_SPI_STATE_READY) {
    }
    HAL_SPI_Transmit_IT(temp.SpiandSlave->SpiHandle, (uint8_t *)temp.data, temp.count * temp.sizeofDataType);
   
 
    if (Writepos <= Readpos+1 ) { // lezen en schrijven gelijk

    Writepos = 0;
    Readpos = 0;
  } 



}

void SPIInterface::writeData(uint32_t addres, uint8_t *data, uint32_t count) {
  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];

  auto hspi = testStruct->SpiHandle;
  Buffer[Writepos & BUFFERSIZE].SpiandSlave = testStruct;
  Buffer[Writepos & BUFFERSIZE].addres = addres;
  Buffer[Writepos & BUFFERSIZE].count = count * 3;
  Buffer[Writepos & BUFFERSIZE].data = (uint32_t*)data;
  Buffer[Writepos & BUFFERSIZE].sizeofDataType = 1;

  //
/*
  if (Writepos == 0 and Readpos == 0) { // als de buffer leeg is start de interrupt  zelf

    selectSlave(testStruct->SlaceSelectArray[addres]); //selceteer zelf
    while (testStruct->SpiHandle->State != HAL_SPI_STATE_READY) {
    }
    uint8_t temptest[]={0};
    HAL_SPI_Transmit_IT(testStruct->SpiHandle, temptest, count * 1);

    Writepos++;

  } else {
    Writepos++;
  }

  //
*/
  
  //selectSlave(temp); 
   HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Transmit(testStruct->SpiHandle, data, count * 3, 100);
  //HAL_SPI_Transmit(testStruct->SpiHandle, &databuffer[1], 1, 100);
  //HAL_SPI_Transmit(testStruct->SpiHandle, &databuffer[2], 1, 100);
  //deselectSlave(temp); 
   HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);

}

void SPIInterface::writeData(uint32_t addres, uint16_t *data, uint32_t count) {
  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];

  uint8_t databuffer[3] = {0x00, ((*data >> 8) & 0xFF), (*data & 0xFF)};

  selectSlave(temp); // HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Transmit(testStruct->SpiHandle, (uint8_t *)data, count * 3, 100);
  //HAL_SPI_Transmit(testStruct->SpiHandle, &databuffer[1], 1, 100);
  //HAL_SPI_Transmit(testStruct->SpiHandle, &databuffer[2], 1, 100);
  deselectSlave(temp); // HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
}
void SPIInterface::writeData(uint32_t addres, uint32_t *data, uint32_t count) {
  //TODO kijken of dit mogelijk met een decorator kan gedaan worden
  //TODO voeg mogelijk een check toe voor de spihandle voor als er geen HAL drivers gebruikt worden
  //if(SpiHandle != nullptr){

  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  selectSlave(temp); // HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Transmit(testStruct->SpiHandle, (uint8_t *)data, count, 100);
  deselectSlave(temp); // HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
}

void *SPIInterface::ReadData(uint32_t addres, uint32_t *data, uint32_t count) {

  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  selectSlave(temp); // HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Receive(testStruct->SpiHandle, (uint8_t *)data, count, 100);
  deselectSlave(temp); //HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
  return 0;
}
void *SPIInterface::ReadData(uint32_t addres, uint8_t *data, uint32_t count) {

  SlaveSelectStruct temp = testStruct->SlaceSelectArray[addres];
  selectSlave(temp); //HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)temp.slaveactivestate);
  HAL_SPI_Receive(testStruct->SpiHandle, data, count, 100);
  deselectSlave(temp); //HAL_GPIO_WritePin(temp.GPIOHanndle, temp.GPIO_Pin, (GPIO_PinState)~temp.slaveactivestate);
  return 0;
}

void SPIInterface::selectSlave(SlaveSelectStruct slave) {
  HAL_GPIO_WritePin(slave.GPIOHanndle, slave.GPIO_Pin, (GPIO_PinState)slave.slaveactivestate);
}
void SPIInterface::deselectSlave(SlaveSelectStruct slave) {

  HAL_GPIO_WritePin(slave.GPIOHanndle, slave.GPIO_Pin, (GPIO_PinState)~slave.slaveactivestate);
}

void SPIInterface::addIRQType(SPIANDGPIOstruct *SPIAndSlave, uint32_t *data) {

//  IRQBuffertype temp = {.SpiandSlave = SPIAndSlave, .data = data};
 // Buffer[Writepos] = temp;
 // Writepos++;
}

SPIInterface::~SPIInterface() {
}
SPIInterface *SPIInterface::clone() {
  return this; //new SPIInterface;
}
void SPIInterface::addHalDriver(void *HalDriver) {
  testStruct = (SPIANDGPIOstruct *)HalDriver;
}

int8_t SPIInterface::addHalDriver(void *HalDriver, bool test) {

  if (SelectStructCount >= 7) {
    return -1;
  }
  int i = 0;
  while (SPISelectStruct[i] != nullptr) {
    i++;
  }
  SPISelectStruct[i] = (SPIANDGPIOstruct *)HalDriver;
  return i;
}

void SPIInterface::DisableInterrupt(SPI_HandleTypeDef *SpiHandle) {
}
void SPIInterface::EnableInterrupt(SPI_HandleTypeDef *SpiHandle) {
}