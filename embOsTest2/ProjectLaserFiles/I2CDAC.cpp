#include "I2CDAC.h" 


#include "HalDriverHandles.h" 
#include "SerialInterfaceFac.h" 

  I2CDAC I2CDAC::Own(I2CDACC);


 I2CDAC::I2CDAC(){



 }
I2CDAC::I2CDAC(DACtypes DriverType):DacDriver(DriverType){

 this->Interface =SerialInterfaceFac::makeInterface(SPI); // negeer dit even voor een minuut


}


void I2CDAC::SetHAlDriver(void* Driver){

this->Interface->addHalDriver(Driver);

}

I2CDAC::~I2CDAC(){
}

void I2CDAC::writeAnalog(uint16_t analogValue, uint8_t dacnumber){ // voor een MAX5216 DAC

uint32_t temp = analogValue<<6;


uint8_t firstbyte = (1<<6)|((temp>>16)&0xFF);
uint8_t secondbyte = (temp>>8)&0xFF;
uint8_t lastbyte = (temp)&0xFF;


uint8_t databuffer[3]={firstbyte,secondbyte,lastbyte};

this->Interface->writeData(dacnumber&1,databuffer,1);


}