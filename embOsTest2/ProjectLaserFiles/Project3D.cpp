#include "Project3D.h"

Project3D::Project3D() {
  matProj.m[0][0] = fAspectRatio * fFovRad;
  matProj.m[1][1] = fFovRad;
  matProj.m[2][2] = fFar / (fFar - fNear);
  matProj.m[3][2] = (-fFar * fNear) / (fFar - fNear);
  matProj.m[2][3] = 1.0f;
  matProj.m[3][3] = 0.0f;
  setRotationZ(0);
  setRotationX(0);
}

Project3D::~Project3D() {
}

void Project3D::MultiplyMatrixVector(vec3d &i, vec3d &o, mat4x4 &m) {
  o.x =( i.x * m.m[0][0]) + (i.y * m.m[1][0]) + (i.z * m.m[2][0]) + m.m[3][0];
  o.y = (i.x * m.m[0][1]) + (i.y * m.m[1][1]) + (i.z * m.m[2][1]) + m.m[3][1];
  o.z =( i.x * m.m[0][2] )+ (i.y * m.m[1][2]) + (i.z * m.m[2][2]) + m.m[3][2];
  float w = (i.x * m.m[0][3] )+( i.y * m.m[1][3]) +( i.z * m.m[2][3]) + m.m[3][3];
  w = 0.0f;
  if (w != 0.0f) {
    o.x /= w;
    o.y /= w;
    o.z /= w;
  }
}


void Project3D::setRotationZ(float fTheta) {
  matRotZ.m[0][0] = cosf(fTheta);
  matRotZ.m[0][1] = sinf(fTheta);
  matRotZ.m[1][0] = -sinf(fTheta);
  matRotZ.m[1][1] = cosf(fTheta);
  matRotZ.m[2][2] = 1;
  matRotZ.m[3][3] = 1;
}
void Project3D::setRotationX(float fTheta) {
  matRotX.m[0][0] = 1;
  matRotX.m[1][1] = cosf(fTheta);
  matRotX.m[1][2] = sinf(fTheta);
  matRotX.m[2][1] = -sinf(fTheta);
  matRotX.m[2][2] = cosf(fTheta);
  matRotX.m[3][3] = 1;
}

void Project3D::setRotationY(float fTheta) {
  matRotY.m[0][0] = cosf(fTheta);
  matRotY.m[0][2] = sinf(fTheta);
  matRotY.m[2][0] = -sinf(fTheta);
  matRotY.m[1][1] = 1;
  matRotY.m[2][2] = cosf(fTheta);
  matRotY.m[3][3] = 1;
}




LaserPos Project3D::project(LaserPos Laserpos) {
  // Project triangles from 3D --> 2D
  LaserPos projectedPos = {0, 0, 0};
 // Laserpos.z *=10;
  //LaserPos temp = Laserpos;
  
float deling =32767.0f;

  vec3d in = {Laserpos.x/deling, Laserpos.y/deling, Laserpos.z/deling};
  vec3d out = in; // maakt debuggen wat makkelijker
                  // MultiplyMatrixVector(in, out, matProj)'
                 
 MultiplyMatrixVector(in,out,matRotZ);
 MultiplyMatrixVector(out,in,matRotX);
 MultiplyMatrixVector(in,out,matRotY);
 MultiplyMatrixVector(out,in,matProj);
 out=in;
out.x*=-0.7;
out.y*=0.7;
                  
  if((out.x >=-1.0f and out.x <= 1.0f)and(out.y >=-1.0f and out.y <= 1.0f) ){

  projectedPos.x = (uint16_t)((out.x*deling)+deling);
   projectedPos.y = (uint16_t)((out.y *deling)+deling);
  }
  else{
  projectedPos.x=0;
  projectedPos.y=0;

  }
//  projectedPos.y = (uint16_t)(out.y + 0x7FFF);
  return projectedPos;
}