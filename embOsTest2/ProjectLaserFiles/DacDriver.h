#pragma once
#include <stdint.h>
#include "DriverEnum.h"
// DAC driver dat 2 dezelfde DAC's aanstuurt.
// Dit kan zijn door een IC dat 2 of meer DAC's bevat, een MCU dat 2 DAC's bevat
// of 2 losse DAC IC's
class DacDriver{
public:
DacDriver();
DacDriver(DACtypes DriverType);
virtual ~DacDriver();

virtual void writeAnalog(uint16_t analogValue, uint8_t dacnumber)=0;
virtual void SetHAlDriver(void* Driver)=0;

};