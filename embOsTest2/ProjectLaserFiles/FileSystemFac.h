
#pragma once
#include "Fat32FileSystem.h" 
class FileSystemFac { // een factory voor het maken van verschillende filesystem. Alleen fat32 geimplementeerd

private:
  
public:
static void addFileSystem(Fat32FileSystem * FilesystemtoUse);
static Fat32FileSystem* getFileSystem();
static Fat32FileSystem* getFileSystemMap();



};