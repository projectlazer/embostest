#pragma once
#include "DacDriver.h"
#include "LaserStructs.h"
#include "DriverEnum.h" 
#include "PWMDriver.h"


//LaserDriver is het gene wat de laser gebruikt.
//De LaserDriver bied een Manier van aansturen van een Laser aan
//Als er zo een laser gebruikt word dat UART ipv SPI gebruikt dan kan er een nieuwe afgeleide klasse gemaakt worden die UART gebruikt
class LaserDriver {

public:
  LaserDriver();
  LaserDriver(LaserType DriverType);
  virtual ~LaserDriver();
  virtual void writeValue(LaserPos laserpos, uint32_t timetopoint) = 0;
  virtual void writeColor(LaserColor Color) = 0;
  virtual void writePoint(LaserCoord coord) = 0;
  virtual void SetTimetoPoint(uint32_t timetoPoint) = 0;
  void setPWM(PWMDriver* driver);
  void setDac(DacDriver* driver,void* Driver);

   PWMDriver *pwm;//temp;
protected:
 DacDriver *dac;


};