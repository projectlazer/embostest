#pragma once
#include "stdint.h"
#include "ExternalStorage.h"
#include <map>
#include "ExternalStorageEnum.h"
#include "SerialInterFaceDriver.h"
class ExternalStorageFac{
private:
 typedef std::map <StrorageType , ExternalStorage*> storagemap; // map om te defineren wat voor ExternalStorage het is

 static storagemap& getStorageMap();

public:

static ExternalStorage* MakeStorage(StrorageType type);
static ExternalStorage* MakeStorage(StrorageType type,SerialInterFaceDriver* interface);

static void AddStorage(StrorageType type, ExternalStorage* storage);


};