#include "PWMDriver.h"
#include "HalDriverHandles.h" 
#include "LaserFac.h" 
PWMDriver PWMDriver::Own;

PWMDriver::PWMDriver():PWMDriver(&htim4) { // hardcoded Driver
}

PWMDriver::~PWMDriver() {
}

PWMDriver::PWMDriver(TIM_HandleTypeDef *driver) {
  addhallDriver(driver);

  LaserFac::addPWM(INTERNALPWM, this);
}

void PWMDriver::addhallDriver(TIM_HandleTypeDef *driver) {
  this->driver = driver;
}
void PWMDriver::writPWMValue(uint8_t analog, uint8_t number) {

  volatile uint32_t *readpos = &this->driver->Instance->CCR1 + (number);
  *readpos = analog;
}
void PWMDriver::writePrescaler(uint16_t prescaler){
  volatile uint32_t *readpos = &this->driver->Instance->PSC;
  *readpos = prescaler;


}