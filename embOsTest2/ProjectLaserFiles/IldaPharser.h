#include "LaserStructs.h"
#include <stdio.h>
#include <string.h>
#pragma once
class IldaPharser {

  // geet een getal terug dat meld of het laaste punt geldigis.
  // een laaste punt is ongeldig als er niet genoeg data beschikbaar is vanuit de data pointer

public:
  IldaPharser();

  IldaHeader getIldaHeader(uint32_t data[8]); // Ilda Header

  //de eerste data byte word gezien als
  uint32_t parseStreamData(uint8_t *data, LaserCoord *parsedData, IldaHeader IldaFormat, uint32_t inputdatacount);
  //parse streamData geeft een aantal punten terug afhankelijk vanaf hoeveel data er in gestopt word.
  // Doordat verschillende formats in grote varieren kan het dus zijn dat als er een format 0 meegegeven word er meer punten terug gegeven kunnen worden dan format 5.
  // groei wat eigen verantwoordelijkheid en hou daar rekening mee
  uint32_t getBytesPerRecord(IldaHeader header);
  virtual ~IldaPharser();

private:
 const uint8_t format0Size = 8;
 const uint8_t format1Size = 6;
 const uint8_t format4Size = 10;
 const uint8_t format5Size = 8;
  uint32_t formatsize[6]= {format0Size,format1Size,0,0,format4Size,format5Size};

  uint32_t parseFormat0(uint8_t *data, LaserCoord *parsedData, uint32_t count);
  uint32_t parseFormat1(uint8_t *data, LaserCoord *parsedData, uint32_t count);
  uint32_t parseFormat4(uint8_t *data, LaserCoord *parsedData, uint32_t count);
  uint32_t parseFormat5(uint8_t *data, LaserCoord *parsedData, uint32_t count);
  struct colorTableIlda {
    uint8_t rood;
    uint8_t groen;
    uint8_t blauw;
  };

  colorTableIlda colortable[64] = {
      {255, 0, 0},
      {255, 16, 0},
      {255, 32, 0},
      {255, 48, 0},
      {255, 64, 0},
      {255, 80, 0},
      {255, 96, 0},
      {255, 112, 0},
      {255, 128, 0},
      {255, 144, 0},
      {255, 160, 0},
      {255, 176, 0},
      {255, 192, 0},
      {255, 208, 0},
      {255, 255, 0},
      {224, 255, 0},
      {192, 255, 0},
      {160, 255, 0},
      {128, 255, 0},
      {96, 255, 0},
      {64, 255, 0},
      {32, 255, 0},
      {0, 255, 0},
      {0, 255, 36},
      {0, 255, 73},
      {0, 255, 109},
      {0, 255, 146},
      {0, 255, 182},
      {0, 255, 219},
      {0, 255, 255},
      {0, 255, 255},
      {0, 227, 255},
      {0, 198, 255},
      {0, 170, 255},
      {0, 142, 255},
      {0, 113, 255},
      {0, 85, 255},
      {0, 56, 255},
      {0, 28, 255},
      {0, 0, 255},
      {32, 0, 255},
      {64, 0, 255},
      {96, 0, 255},
      {128, 0, 255},
      {160, 0, 255},
      {192, 0, 255},
      {224, 0, 255},
      {255, 0, 255},
      {255, 0, 255},
      {255, 32, 255},
      {255, 64, 255},
      {255, 96, 255},
      {255, 128, 255},
      {255, 160, 255},
      {255, 192, 255},
      {255, 224, 255},
      {255, 255, 255},
      {255, 224, 224},
      {255, 192, 192},
      {255, 160, 160},
      {255, 128, 128},
      {255, 96, 96},
      {255, 64, 64},
      {255, 32, 32}};
};