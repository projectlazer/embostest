#include "ExternalStorage.h"
#include "Fat32FileSystem.h"
#include "FileSystem.h"
#include "SDIOInterface.h"
#include <string>
class SDCard : public ExternalStorage {
public:
  virtual ~SDCard();
  SDCard(SerialInterFaceDriver *interface);
  void StreamFile(std::string filename, uint8_t *buffer, uint32_t startpoint, uint32_t size);
  void ReadFile(std::string filename, uint8_t *buffer);
  void ListFilesByExtension(std::string extension, FileInfo *FileNames, uint32_t maxcount);
  void getFileInfo(std::string filename);
  void addInterFace(SerialInterFaceDriver *interface);
  SDCard *clone();
  SDCard *clone(SerialInterFaceDriver *interface);

private:
  SDCard();
  Fat32FileSystem *system;



  static SDCard Own;
};