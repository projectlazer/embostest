
#pragma once
enum LaserDriveType { GreenLaserDriver,
  RGBLaserDriver,
  VirtualLaserDriver };
typedef enum { SPIDACC ,I2CDACC} DACtypes;
typedef enum { INTERNALPWM } PWMTypes;
typedef enum { LaserRGB,
  LaserGreen,
  LaserVirtual } LaserType;

struct LaserConfigs {

 
  PWMTypes PWM;
  DACtypes Dac;
  LaserType laser;
   void* HalDriver;
};