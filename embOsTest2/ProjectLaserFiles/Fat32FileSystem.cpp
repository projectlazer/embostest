#include "Fat32FileSystem.h"
#include "RTOS.h"
#include <stdio.h>
#include <string.h>

Fat32FileSystem::Fat32FileSystem(SerialInterFaceDriver *driver) {

  this->driver = driver;
  BufferdFAT = (bufferdFATSectorType *)OS_HEAP_malloc((this->FatBufferSize) * sizeof(bufferdFATSectorType));
  bufferdFATSectorType *reader = BufferdFAT;
  for (int i = 0; i < this->FatBufferSize; i++) {
    reader->hitcount = 0;
    reader->SectorNummer = -1;
    reader++;
  }

  getFileSystemInfo();
  FindFirstDataCluster();
}
Fat32FileSystem::~Fat32FileSystem() {
}

void Fat32FileSystem::ReadFile(std::string Filename, uint8_t *buffer) {
}
void Fat32FileSystem::getFileInfo(std::string Filename) {
}
void Fat32FileSystem::StreamFile(std::string Filename, uint32_t *buffer, uint32_t startpoint, uint32_t size) {
  fileStruct tempStruct;
  int32_t error = FindFile(Filename, &tempStruct);
  uint32_t tempsize = size;
  uint32_t allocbuffersize = 0;
  if (error != -1) {
    uint32_t bufferskipped = startpoint & this->bootSector.BPB_bytsPerSec - 1; //TODO wekt mogelij niet

    if (bufferskipped + (size % this->bootSector.BPB_bytsPerSec) > this->bootSector.BPB_bytsPerSec) {
      allocbuffersize = ((size / this->bootSector.BPB_bytsPerSec) + 2) * this->bootSector.BPB_bytsPerSec;
      tempsize += this->bootSector.BPB_bytsPerSec;
    } else {
      allocbuffersize = ((size / this->bootSector.BPB_bytsPerSec) + 1) * this->bootSector.BPB_bytsPerSec; // hoevaak komt 512 voor kan netter door de sector grote van het de bootsector op te vragen
    }
    uint8_t *tempbuffer = (uint8_t *)OS_HEAP_malloc((allocbuffersize) * sizeof(uint8_t)); //misra?

    getDataFromFileOptimezed(tempStruct, (uint32_t *)tempbuffer, startpoint, tempsize);
    tempbuffer += bufferskipped;

    memcpy(buffer, tempbuffer, size);
    tempbuffer -= bufferskipped; //scary
    OS_HEAP_free(tempbuffer);
  }
}

void Fat32FileSystem::StreamFile(FileInfo Filename, uint32_t *buffer, uint32_t startpoint, uint32_t size) {
  while (startpoint + size > Filename.sizeInBytes) {
  }
  StreamFile(Filename.name, buffer, startpoint, size);
}
// limitatie is dat er iedere keer opnieuw vanaf het begin gelezen moet worden door de cluster chain
void Fat32FileSystem::getDataFromFile(fileStruct file, uint32_t *buffer, uint32_t startpoint, uint32_t count) { // hou startpoint voor nu een meervoud van 8192
  uint32_t clustersize = (this->bootSector.BPB_SecPerClus);
  uint32_t roundendupcount = ((count / this->bootSector.BPB_bytsPerSec) + 1) * this->bootSector.BPB_bytsPerSec;
  uint32_t roundedDownStartpoint = ((startpoint / this->bootSector.BPB_bytsPerSec)) * this->bootSector.BPB_bytsPerSec;
  uint32_t firstincluster = (roundedDownStartpoint / this->bootSector.BPB_bytsPerSec) % clustersize;
  uint32_t FileFirstCluster = (file.DIR_FstClusHI << 16) | (file.DIR_FstClusLow & 0xFFFF);
  uint32_t ClusterPosToSector = this->FirstDataSectorL + ((FileFirstCluster - 2) * clustersize);

  uint32_t remaindercounttogo = roundendupcount;

  uint32_t firstLoc = (startpoint / this->clusterSize); // hoevaak moet er gesprongen worden voordat er er bij de eerste gewilde cluster gekomen kan worden
  uint32_t jumpcount = (count) / this->clusterSize;     // hoevaak moet er gesprongen worden.
  uint32_t firstsectorincluster = startpoint / this->bootSector.BPB_bytsPerSec;
  uint32_t hoogstesectornummer;
  uint32_t FATBuffer[512 >> 2];
  uint8_t *writerpos = (uint8_t *)buffer;

  uint32_t bytespercluster = clustersize * this->bootSector.BPB_bytsPerSec;
  uint32_t lastSectornumberinclustercount = (((count - 1) - startpoint) / this->bootSector.BPB_bytsPerSec) % (clustersize - 1);

  uint32_t firstclusternumber = (file.DIR_FstClusHI << 16) | (file.DIR_FstClusLow & 0xFFFF); // zoek cluster nummer op
  hoogstesectornummer = this->FirstDataSectorL + ((firstclusternumber - 2) * clustersize);   // de cluster tabel bevind zich direct na de firstdataSector

  if (roundedDownStartpoint < bytespercluster) { // als de eerste gewenste data binnen de eerste cluster valt

    uint32_t maxToEnd = ((bytespercluster - roundedDownStartpoint) / this->bootSector.BPB_bytsPerSec);

    if (maxToEnd > (roundendupcount / this->bootSector.BPB_bytsPerSec)) { // als er minder gelezen
      maxToEnd = (roundendupcount / this->bootSector.BPB_bytsPerSec);
    }

    driver->ReadData(ClusterPosToSector + firstincluster, writerpos, maxToEnd);
    writerpos += maxToEnd * this->bootSector.BPB_bytsPerSec;            // verhoog de lees pointer met het aantal gelezen bytes.
    remaindercounttogo -= (maxToEnd) * this->bootSector.BPB_bytsPerSec; // nog zoveel moet er gelezen worden
    firstincluster = 0;
  }
  if (remaindercounttogo > 0) {                    // als er nog meer data gelezen moet worden
    FATChain fileFatChain = findFATClusters(file); // krijg het secotor en offset numer van de file
    driver->ReadData(fileFatChain.sector, FATBuffer, 1);
    uint32_t readeroffset = fileFatChain.offsetInSector >> 2;
    uint32_t fatdiffrence = FATBuffer[readeroffset];
    // eerste gelezen cluster
    for (int i = 1; i < roundedDownStartpoint / bytespercluster; i++) { // loop door de clustertable heen
      readeroffset++;
      if (readeroffset > 127) {
        fileFatChain.sector++;
        driver->ReadData(fileFatChain.sector, FATBuffer, 1);
        readeroffset = 0;
      }
    }
    while (remaindercounttogo > 0) {
      uint32_t foundclusternumber = FATBuffer[readeroffset];
      uint32_t foundsectornumber = this->FirstDataSectorL + ((foundclusternumber - 2) * clustersize);
      uint32_t sectorSize = (remaindercounttogo / this->bootSector.BPB_bytsPerSec);
      if (sectorSize > clustersize) {
        sectorSize = clustersize;
      }
      driver->ReadData(foundsectornumber + firstincluster, writerpos, sectorSize);
      remaindercounttogo -= sectorSize * this->bootSector.BPB_bytsPerSec;
      writerpos += (sectorSize * this->bootSector.BPB_bytsPerSec);
      readeroffset++;
      if (readeroffset > 127) {
        fileFatChain.sector++;
        driver->ReadData(fileFatChain.sector, FATBuffer, 1);
        readeroffset = 0;
      }
    }
  }
}

void Fat32FileSystem::getDataFromFileOptimezed(fileStruct file, uint32_t *buffer, uint32_t startpoint, uint32_t count) { // hou startpoint voor nu een meervoud van 8192
  uint32_t clustersize = (this->bootSector.BPB_SecPerClus);
  uint32_t roundendupcount = ((count / this->bootSector.BPB_bytsPerSec) + 1) * this->bootSector.BPB_bytsPerSec;
  uint32_t roundedDownStartpoint = ((startpoint / this->bootSector.BPB_bytsPerSec)) * this->bootSector.BPB_bytsPerSec;
  uint32_t firstincluster = (roundedDownStartpoint / this->bootSector.BPB_bytsPerSec) % clustersize;
  uint32_t FileFirstCluster = (file.DIR_FstClusHI << 16) | (file.DIR_FstClusLow & 0xFFFF);
  uint32_t ClusterPosToSector = this->FirstDataSectorL + ((FileFirstCluster - 2) * clustersize);

  uint32_t remaindercounttogo = roundendupcount;

  uint32_t firstLoc = (startpoint / this->clusterSize); // hoevaak moet er gesprongen worden voordat er er bij de eerste gewilde cluster gekomen kan worden
  uint32_t jumpcount = (count) / this->clusterSize;     // hoevaak moet er gesprongen worden.
  uint32_t firstsectorincluster = startpoint / this->bootSector.BPB_bytsPerSec;
  uint32_t hoogstesectornummer;
  uint32_t FATBuffer[512 >> 2];
  uint8_t *writerpos = (uint8_t *)buffer;

  uint32_t bytespercluster = clustersize * this->bootSector.BPB_bytsPerSec;
  uint32_t lastSectornumberinclustercount = (((count - 1) - startpoint) / this->bootSector.BPB_bytsPerSec) % (clustersize - 1);

  uint32_t firstclusternumber = (file.DIR_FstClusHI << 16) | (file.DIR_FstClusLow & 0xFFFF); // zoek cluster nummer op
  hoogstesectornummer = this->FirstDataSectorL + ((firstclusternumber - 2) * clustersize);   // de cluster tabel bevind zich direct na de firstdataSector

  if (roundedDownStartpoint < bytespercluster) { // als de eerste gewenste data binnen de eerste cluster valt

    uint32_t maxToEnd = ((bytespercluster - roundedDownStartpoint) / this->bootSector.BPB_bytsPerSec);

    if (maxToEnd > (roundendupcount / this->bootSector.BPB_bytsPerSec)) { // als er minder gelezen
      maxToEnd = (roundendupcount / this->bootSector.BPB_bytsPerSec);
    }

    driver->ReadData(ClusterPosToSector + firstincluster, writerpos, maxToEnd);
    writerpos += maxToEnd * this->bootSector.BPB_bytsPerSec;            // verhoog de lees pointer met het aantal gelezen bytes.
    remaindercounttogo -= (maxToEnd) * this->bootSector.BPB_bytsPerSec; // nog zoveel moet er gelezen worden
    firstincluster = 0;
  }
  if (remaindercounttogo > 0) { // als er nog meer data gelezen moet worden

    FATChain fileFatChain = findFATClusters(file); // krijg het secotor en offset numer van de file
    getFatSector(FATBuffer, fileFatChain.sector);
    uint32_t readeroffset = fileFatChain.offsetInSector >> 2;
    uint32_t fatdiffrence = FATBuffer[readeroffset];
    uint32_t oldsector = fileFatChain.sector;
    for (int i = 1; i < roundedDownStartpoint / bytespercluster; i++) { // loop door de clustertable heen

      fileFatChain = findFATClusters(fatdiffrence);
      if (oldsector != fileFatChain.sector) { //
        getFatSector(FATBuffer, fileFatChain.sector);
        oldsector = fileFatChain.sector;
      }
      readeroffset = fileFatChain.offsetInSector >> 2;
      fatdiffrence = FATBuffer[readeroffset];
    }
    while (remaindercounttogo > 0) {
      uint32_t foundclusternumber = FATBuffer[readeroffset];
      uint32_t foundsectornumber = this->FirstDataSectorL + ((foundclusternumber - 2) * clustersize);
      uint32_t sectorSize = (remaindercounttogo / this->bootSector.BPB_bytsPerSec);
      if (sectorSize > clustersize) {
        sectorSize = clustersize;
      }
      driver->ReadData(foundsectornumber + firstincluster, writerpos, sectorSize);
      remaindercounttogo -= sectorSize * this->bootSector.BPB_bytsPerSec;
      writerpos += (sectorSize * this->bootSector.BPB_bytsPerSec);
      fileFatChain = findFATClusters(fatdiffrence);
      if (oldsector != fileFatChain.sector) { //
        getFatSector(FATBuffer, fileFatChain.sector);
        oldsector = fileFatChain.sector;
      }
      readeroffset = fileFatChain.offsetInSector >> 2;
      fatdiffrence = FATBuffer[readeroffset];
    }
  }
}

Fat32FileSystem::FATChain Fat32FileSystem::findFATClusters(fileStruct file) {
  uint32_t firstclusternumber = (file.DIR_FstClusHI << 16) | (file.DIR_FstClusLow & 0xFFFF);
  uint32_t sectornumber = this->BootSectorL + this->bootSector.BPB_RscdSecCnt + ((firstclusternumber * 4) / this->bootSector.BPB_bytsPerSec);
  uint32_t offsetinSector = (firstclusternumber * 4) % this->bootSector.BPB_bytsPerSec;
  return {sectornumber, offsetinSector};
}

Fat32FileSystem::FATChain Fat32FileSystem::findFATClusters(uint32_t clusternummer) {
  uint32_t sectornumber = this->BootSectorL + this->bootSector.BPB_RscdSecCnt + ((clusternummer * 4) / this->bootSector.BPB_bytsPerSec);
  uint32_t offsetinSector = (clusternummer * 4) % this->bootSector.BPB_bytsPerSec;
  return {sectornumber, offsetinSector};
}

int32_t Fat32FileSystem::FindFile(std::string Filename, fileStruct *file) {

  // quick fix om sneller te maken
  // doet niks met buffers vullen of met folders.
  //op met moment is het gebruik van folders opgegeven
  auto temp = this->FileBuffer.find(Filename);

  if (temp != FileBuffer.end()) { //
    memcpy_fast(file, &temp->second, sizeof(fileStruct));

    return 4;
  }

  uint32_t buffer[512 >> 2];
  uint32_t offset = 0;
  uint32_t count = 0;
  this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
  uint32_t i = 0;
  fileStruct *fileStructReader = (fileStruct *)&buffer;
  while (fileStructReader->DIR_Name[0] > 0x5) {
    while (fileStructReader->DIR_Att == 0xF) { // als er een long entry aanwezig is
      fileStructReader++;
      i++;
      if (i > 15) {
        offset++;
        this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
        fileStructReader = (fileStruct *)&buffer;
        i = 0;
      }
    }
    std::string temp = "";
    temp.append(fileStructReader->DIR_Name);
    if (Filename.compare(temp) == 0) {
      memcpy(file, fileStructReader, sizeof(fileStruct));
      //*file = *fileStructReader;
      return (offset + 1) * i;
    }
    fileStructReader++;
    i++;
    if (i > 15) {
      offset++;
      this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
      fileStructReader = (fileStruct *)&buffer;
      i = 0;
    }
  }
  return -1;
}
void Fat32FileSystem::getFileSystemInfo() {

  uint32_t ReadLoc = 0x0000;
  uint32_t buffer[512 >> 2];
  this->driver->ReadData(ReadLoc, buffer, 1);
  bootSectorStruct *readPointer = (bootSectorStruct *)&buffer;
  while (readPointer->BS_BootSig != 0x29) {
    ReadLoc += 0x1000; //TODO +=0x1000 is om het tijdelijk te vernsellen omdat de 2 gekende boot locaties op 0x0 of 0x2000 staan. Dit mischien ook 0x14678 of 0x0968 zijn
    this->driver->ReadData(ReadLoc, buffer, 1);
  }
  // bootSectorStruct *readPointerSectorReader = &bootSector;
  // *readPointerSectorReader = *readPointer;
  // this->bootSector = *readPointer; //(564 clock cycles met memcpy)
  memcpy(&this->bootSector, readPointer, sizeof(bootSectorStruct));
  this->BootSectorL = ReadLoc;
  this->clusterSize = this->bootSector.BPB_SecPerClus * this->bootSector.BPB_bytsPerSec;
}

void Fat32FileSystem::FindFirstDataCluster() {
  RootDataSectorL = ((this->bootSector.BPB_RootEntCount * 32) + (this->bootSector.BPB_bytsPerSec - 1)) / this->bootSector.BPB_bytsPerSec;
  while (RootDataSectorL != 0) {
    int a = 100; //error state
  }
  FirstDataSectorL = BootSectorL + this->bootSector.BPB_RscdSecCnt + (this->bootSector.BPB_NumFats * this->bootSector.BPB_FATSz32) + RootDataSectorL; //eerste data sector is relatief aan de locatie van de boot sector
}

uint32_t Fat32FileSystem::countAllextension(std::string Extension) {
  uint32_t buffer[512 >> 2];
  uint32_t offset = 0;
  uint32_t count = 0;
  this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
  uint32_t i = 0;
  fileStruct *fileStructReader = (fileStruct *)&buffer;
  while (fileStructReader->DIR_Name[0] > 0x5) {
    while (fileStructReader->DIR_Att == 0xF) { // als er een long entry aanwezig is
      fileStructReader++;
      i++;
      if (i > 15) {
        offset++;
        this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
        fileStructReader = (fileStruct *)&buffer;
        i = 0;
      }
    }
    std::string temp;
    temp.append(fileStructReader->DIR_Name);
    if (Extension.compare(temp.substr(8, 3)) == 0) {
      count++;
    }
    fileStructReader++;
    i++;
    if (i > 15) {
      offset++;
      this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
      fileStructReader = (fileStruct *)&buffer;
      i = 0;
    }
  }
  return count;
}
void Fat32FileSystem::listAllFilesWithExtension(std::string Extension, FileInfo *StructArray, uint32_t maxcountoffiles) {

  uint32_t buffer[512 >> 2];
  uint32_t offset = 0;
  uint32_t count = 0;
  this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
  uint32_t i = 0;
  fileStruct *fileStructReader = (fileStruct *)&buffer;
  while (count < maxcountoffiles) {
    while (fileStructReader->DIR_Att == 0xF) { // als er een long entry aanwezig is
      fileStructReader++;
      i++;
      if (i > 15) {
        offset++;
        this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
        fileStructReader = (fileStruct *)&buffer;
        i = 0;
      }
    }
    if (fileStructReader->DIR_Name[0] > 0x5 && fileStructReader->DIR_Name[0] != 0xE5) {
      std::string temp;
      temp.append(fileStructReader->DIR_Name);
      if (temp.find_first_of('.') == 0) {
        return;
      }

      if (Extension.compare(temp.substr(8, 3)) == 0) {
        StructArray[count].name = temp;
        StructArray[count].sizeInBytes = fileStructReader->DIR_FileSize;
        this->FileBuffer.insert(std::pair<std::string, fileStruct>(temp, *fileStructReader));
        count++;
      }
    }
    fileStructReader++;
    i++;
    if (i > 15) {
      offset++;
      this->driver->ReadData(FirstDataSectorL + offset, buffer, 1);
      fileStructReader = (fileStruct *)&buffer;
      i = 0;
    }
  }
}

int Fat32FileSystem::getBufferFat(uint32_t sector) {
  bufferdFATSectorType *reader = BufferdFAT;
  for (int i = 0; i < this->FatBufferSize; i++) {
    if (reader->SectorNummer == sector) {

      return i;
    }
    reader++;
  }
  return -1;
}

void Fat32FileSystem::AddFATtoBuffer(uint32_t *Buffer, bufferdFATSectorType FAT) {
  int lowestUse = INT32_MAX;
  int lowestPos = 0;
  bufferdFATSectorType *reader = BufferdFAT;
  bufferdFATSectorType *writer = BufferdFAT;
  for (int i = 0; i < this->FatBufferSize; i++) {
    if (reader->hitcount < lowestUse) {

      lowestUse = reader->hitcount;
      lowestPos = i;
      writer = reader;
    }
    reader++;
  }

  memcpy_fast(&writer->Sector, Buffer, 512);
  writer->SectorNummer = FAT.SectorNummer;
  writer->hitcount = 1;
}

void Fat32FileSystem::AddFATtoBuffer(uint32_t *Buffer, uint32_t sector) {
  bufferdFATSectorType Fat;
  Fat.SectorNummer = sector;

  AddFATtoBuffer(Buffer, Fat);
}

void Fat32FileSystem::getFatSector(uint32_t *Buffer, uint32_t sector) {
  uint32_t BufferFatNumber = this->getBufferFat(sector);
  bufferdFATSectorType *reader = BufferdFAT;
  if (BufferFatNumber != -1) {
    reader += BufferFatNumber; //scary
    //Buffer = reader->Sector;
    reader->hitcount++;
    memcpy_fast(Buffer, reader->Sector, this->bootSector.BPB_bytsPerSec);
  } else {
    driver->ReadData(sector, Buffer, 1);
    AddFATtoBuffer(Buffer, sector);
  }
}

/*
void Fat32FileSystem::ReadData(uint32_t addres, uint8_t *data, uint32_t count){
this->driver->ReadData(addres+this->FirstDataSectorL,data,count);

}
void Fat32FileSystem::ReadData(uint32_t addres, uint32_t *data, uint32_t count){
this->driver->ReadData(addres+this->FirstDataSectorL,data,count);

}
*/