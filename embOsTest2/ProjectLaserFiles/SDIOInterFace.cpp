#include "SDIOInterFace.h"
#include <vector>

SDIOInterFace SDIOInterFace::Own;

void HAL_SD_RxCpltCallback(SD_HandleTypeDef *hsd) {

  HAL_SD_GetCardState(hsd);
  SDIOInterFace::Own.dataReady = 1;
}

SDIOInterFace::SDIOInterFace() : SerialInterFaceDriver(SDIO) {
}
SDIOInterFace::~SDIOInterFace() {
}

void SDIOInterFace::writeData(uint32_t addres, uint8_t *data, uint32_t count) {
}
void SDIOInterFace::writeData(uint32_t addres, uint16_t *data, uint32_t count) {
}
void SDIOInterFace::writeData(uint32_t addres, uint32_t *data, uint32_t count) {
}
void *SDIOInterFace::ReadData(uint32_t addres, uint32_t *data, uint32_t count) {

 
  //SCB_CleanInvalidateICache();

  HAL_StatusTypeDef temp = HAL_ERROR;
  uint32_t errorcount = 0;
  while (temp != HAL_OK) {
     SCB_CleanInvalidateDCache();
    temp = HAL_SD_ReadBlocks_DMA(SDIOHandle, (uint8_t *)data, addres, count);
    HAL_SD_GetCardState(SDIOHandle);
    errorcount++;
    if (errorcount == 100) {
      errorcount = 100;
    }
  }

  // HAL_StatusTypeDef temp = HAL_SD_ReadBlocks(SDIOHandle, (uint8_t *)data, addres, count,100);
  this->dataReady = 0;
  while (SDIOHandle->State != HAL_SD_STATE_READY) { //and SDIOHandle->Lock != HAL_UNLOCKED) {
  }

  return (void *)temp;
}
void *SDIOInterFace::ReadData(uint32_t addres, uint8_t *data, uint32_t count) {

  //SCB_CleanInvalidateICache();

  HAL_StatusTypeDef temp = HAL_ERROR;
  uint32_t errorcount = 0;
  while (temp != HAL_OK) {
      SCB_CleanInvalidateDCache();
    temp = HAL_SD_ReadBlocks_DMA(SDIOHandle, data, addres, count);
    HAL_SD_GetCardState(SDIOHandle);
    errorcount++;
    while (errorcount >= 100) {
      errorcount = 100;
    }
  }

  // HAL_StatusTypeDef temp = HAL_SD_ReadBlocks(SDIOHandle, (uint8_t *)data, addres, count,100);
  this->dataReady = 0;
  while (SDIOHandle->State != HAL_SD_STATE_READY) { //and SDIOHandle->Lock != HAL_UNLOCKED) {
  }

  return (void *)temp;
}

SDIOInterFace *SDIOInterFace::clone() {
  return this;
  // return new SDIOInterFace; // meerdere SDinterfaces mogelijk SDMMC0 en SDMMC1
}
void SDIOInterFace::addHalDriver(void *HalDriver) {
  SDIOHandle = (SD_HandleTypeDef *)HalDriver;
}