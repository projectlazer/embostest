#include "stdint.h"
#pragma once

typedef struct __attribute__((packed)) IldaHeader{
char ILDA[4];
uint8_t reserved[3];
uint8_t FormatCode;
char FrameName[8];
char CompanyName[8];
uint16_t NumberOfRecords;
uint16_t FrameNumber;
uint16_t totalFrames;
uint8_t ProjectorNumber;
uint8_t reserved2;
} ;


 typedef struct __attribute__((packed)) LaserPos {
    int16_t x;
    int16_t y;
    int16_t z;
  };

  typedef struct __attribute__((packed)) LaserColor {

    uint8_t red;
    uint8_t green;
    uint8_t blue;

  };

  typedef struct __attribute__((packed)) LaserCoord {

    LaserPos Pos;
    LaserColor Color;
  };