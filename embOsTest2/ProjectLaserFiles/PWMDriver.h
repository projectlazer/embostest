#pragma once
#include "stm32h7xx_hal.h"
// geen abstracte klasse van gemaakt omdat een externe PWM IC totaal niet gebruikt zal worden
class PWMDriver {
public:
  PWMDriver();
  PWMDriver(TIM_HandleTypeDef *driver);
  virtual ~PWMDriver();
  void addhallDriver(TIM_HandleTypeDef *driver);
  void writPWMValue(uint8_t analog, uint8_t number);
  void writeCarrier(uint8_t value);
  void writePrescaler(uint16_t prescaler);
private:
  TIM_HandleTypeDef *driver;
  static PWMDriver Own;
};