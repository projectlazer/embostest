#pragma once

#include "IldaPharser.h" 
#include "LaserDriver.h"
#include "LaserStructs.h"
#include "Fat32FileSystem.h" 
#include "Project3D.h"
#include <string>

//De Laser klasse is de interface naar de LaserDriver toe.
// de "gebruiker"van de laser hoeft niet te weten hoe de laser aangestuurt word.
// wel moet de gebruiker de gebruikte LaserDriver en Filesystem mee geven.
//De laser bepaal afhankelijk vanaf een mode variable of er een ILDA bestand of een gegenereerde afbeelding getoont word
// het geeft geen ondersteuning om ILDA bestanden terug te geven aan de "gebruiker"
// dit is omdat er al een filesysteem mee gegeven moet worden
// wel houd het intern de vorige,gebruikte en volgende bestands naam beschikbaar
class Laser {

#define BUFFERSIZE 1
public:
  enum LaserModes { ILDAFILE,
    LISASOE,
    CIRCLE,
    CUBE,
    MAXSQUARE };
  Laser(LaserConfigs config,Fat32FileSystem* system);
  virtual ~Laser();
  void nextIlda();
  void prevIlda();
  void selectIlda(std::string FileName);
  void selectMode(LaserModes Mode);
  //void testpoint(LaserCoord *Coord);
  void testpoint2(LaserCoord *Coord);
  void nextPoint(); //externe interupt meld dat er een nieuw punt getekent moet worden
  void testPrescaler(uint16_t prescalewaarde);
  void setRotation(float x, float y, float z);
  IldaPharser *ilda;
  void IoHandler();


private:
  void nextFrame(); // voor ilda bestanden
  Project3D *Projecttor;
  LaserDriver *Driver;
  Fat32FileSystem *filesystem;
  uint32_t buffer[12]; // interne buffer voor het lezen van de SD kaart

  //Fat32FileSystem::FileInfo Filenames[3];
  LaserModes Mode = MAXSQUARE;
  
  IldaHeader CurrentHeader;
  uint32_t readpos = 0;
  uint32_t writepos = 0;
  uint32_t buffersize = BUFFERSIZE;
  bool nextpointRequested = false;
  bool nextframeRequested = false;
  uint32_t frametime;// geen framerate
  uint32_t CoordBufferReadPos=0;
  uint32_t CoordBufferWritePos=0;
  uint32_t ActiveReaderBuffer =0;
  LaserCoord CoordBuffer[BUFFERSIZE];  //2 buffers voor het uitlezen van de laser coordinaten
                                          // vanuit 1 word er gelezen terwijl er in de ander geschreven word
};