#pragma once
#include "LaserDriver.h"

#include "DriverEnum.h" 
#include <map>
 class LaserFac { 
private:
  
  typedef std::map<LaserDriveType, LaserDriver*> LaserDriverMap; 
  typedef std::map<LaserType, LaserDriver*> LaserDriverMap2; 
  typedef std::map<PWMTypes, PWMDriver*> PWMMap;
  typedef std::map<DACtypes, DacDriver*> DACMap;
 typedef struct {
  LaserDriverMap2 laserMap;
  PWMMap PWMap;
  DACMap DCMap;

  }LaserConfigMap;


public:
   static LaserDriverMap& getLaserMap();

static LaserConfigMap& getConfigMap();


  int test=10;
 
  static LaserDriver* makelaser(LaserDriveType Driverype); // Laser word bij het maken geselecteerd.
  static void choseLaser(LaserDriveType type,LaserDriver *laserdriver);
  static LaserDriver* makeConfig(LaserConfigs config);

private:
  static void addDac(DACtypes type, DacDriver *Driver);
  static void addPWM(PWMTypes type, PWMDriver *Driver);
  static void addLaser(LaserType type, LaserDriver *Driver);

  friend class PWMDriver; // om PWM Driverer toegang te geven tot de addPWM methode
  friend class DacDriver;
  friend class LaserDriver;

};