//#include "ExternalStorageInterface.h"
#pragma once
#include "ExternalStorageEnum.h"
#include "SerialInterFaceDriver.h"
#include "File.h"
#include "stdint.h"
#include <string>
#include <vector>

class ExternalStorage {
public:
  struct FileInfo {
    std::string name;
    uint32_t sizeinbytes;
  };
  ExternalStorage();
  ExternalStorage(StrorageType type);
  virtual void StreamFile(std::string filename, uint8_t *buffer, uint32_t startpoint, uint32_t size) = 0;
  virtual void ReadFile(std::string filename, uint8_t *buffer) = 0;
  virtual void ListFilesByExtension(std::string extension, FileInfo *FileNames, uint32_t maxcount) = 0;
  virtual void getFileInfo(std::string filename) = 0;
  virtual ExternalStorage *clone() = 0;
  virtual ExternalStorage *clone(SerialInterFaceDriver *interface) = 0;
  virtual ~ExternalStorage();
  virtual void addInterFace(SerialInterFaceDriver *interface) = 0;

  SerialInterFaceDriver *SerialDriver;
  File *FileToRead;

  
protected:


private:

};