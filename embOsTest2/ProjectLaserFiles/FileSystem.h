#pragma once
#include <string>
#include "stdint.h"
class FileSystem {
enum FileSystemtype{FAT32};
public:

  struct FileInfo{
  std::string name;
  uint32_t sizeInBytes;
};




  FileSystem();
  FileSystem(FileSystemtype type);
 virtual void ReadFile(std::string Filename,uint8_t* buffer)=0;
  virtual ~FileSystem();
  virtual void getFileSystemInfo()=0;
  virtual void getFileInfo(std::string Filename)=0;
  virtual void StreamFile(std::string Filename,uint32_t* buffer,uint32_t startpoint,uint32_t size)=0;

private:
  //virtual void FindFile(std::string Filename)=0;
};
