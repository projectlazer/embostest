#include "SPIDAC.h" 


#include "HalDriverHandles.h" 
#include "SerialInterfaceFac.h" 

 SPIDAC SPIDAC:: Own(SPIDACC);


 SPIDAC::SPIDAC(){



 }
SPIDAC::SPIDAC(DACtypes DriverType):DacDriver(DriverType){

 this->Interface =SerialInterfaceFac::makeInterface(SPI);


}


void SPIDAC::SetHAlDriver(void* Driver){

this->Interface->addHalDriver(Driver);
this->Interface->addHalDriver(Driver,true);

}

SPIDAC::~SPIDAC(){
}

void SPIDAC::writeAnalog(uint16_t analogValue, uint8_t dacnumber){ // voor een AD5662 DAC

uint8_t databuffer[3]={0x00,((analogValue>>8)&0xFF),(analogValue&0xFF)};

this->Interface->writeData(dacnumber&1,databuffer,1);


}