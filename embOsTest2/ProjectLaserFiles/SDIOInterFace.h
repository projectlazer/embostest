#include "SerialInterFaceDriver.h"
#include "stm32h7xx_hal.h"

class SDIOInterFace : public SerialInterFaceDriver{

public:
SDIOInterFace();
virtual ~SDIOInterFace();
  void writeData(uint32_t addres, uint8_t *data, uint32_t count); //32bit count
  void writeData(uint32_t addres, uint16_t *data, uint32_t count);
  void writeData(uint32_t addres, uint32_t *data, uint32_t count);
  void* ReadData(uint32_t addres, uint32_t *data, uint32_t count); //lezen word altijd naar een 32 bit data pointer gedaan.
  void* ReadData(uint32_t addres, uint8_t *data, uint32_t count); //lezen word altijd naar een 32 bit data pointer gedaan.
  SDIOInterFace *clone();
  void addHalDriver(void *HalDriver);
  int8_t addHalDriver( void* HalDriver,bool ){
  return -1;
  }
  void testfucntion();
  uint8_t dataReady=0;
  static SDIOInterFace Own;
 private:
 SD_HandleTypeDef *SDIOHandle = nullptr;
  
  


};