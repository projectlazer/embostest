#include "LaserDriver.h" 
class RGBLaser : public LaserDriver{


  RGBLaser();
  RGBLaser(LaserType DriverType);
  virtual ~RGBLaser();
   void writeValue(LaserPos laserpos, uint32_t timetopoint);
   void writeColor(LaserColor Color);
   void writePoint(LaserCoord coord) ;
   void SetTimetoPoint(uint32_t timetoPoint) ;

private:
static RGBLaser Own;





};