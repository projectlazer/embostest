#pragma  once
#include "SerialInterFaceDriver.h"
#include <map>

#include <string>


class Fat32FileSystem{// : public FileSystem {

private:

struct bufferdFATSectorType{
 
 uint32_t Sector[512 >> 2];
 uint32_t SectorNummer;
 uint32_t hitcount;
};


  struct FATChain{
    uint32_t sector;
    uint32_t offsetInSector;

  };
struct __attribute__((packed)) fileStruct{
    char DIR_Name[11];          //shortname
    uint8_t DIR_Att;            // attributen zoals read only
    uint8_t DIR_NTRes;          //?
    uint8_t DIR_CrtTimeTenth;   // Milisectonden timestamp (bevat waarden 0 t/m 199)
    uint16_t DIR_CrtTime;       // Tijd waneer het bestand gecreeerd is
    uint16_t DIR_CrtDate;       // datum waarom het bestand gecreerd is
    uint16_t DIR_LstAccDate;    // Laaste acces datum
    uint16_t DIR_FstClusHI;     // de 16 Most significate bits voor het eerste cluster nummer
    uint16_t DIR_WrtTime;       //Tijd van laaste write
    uint16_t DIR_WrtDate;       //datum van laaste tijd
    uint16_t DIR_FstClusLow;    // de 16 Least significate bits voor het eerste cluster nummer
    uint32_t DIR_FileSize;      // Filegrote in bytes
  };


  struct __attribute__((packed)) bootSectorStruct {
    // __attribute__ packed zodat alle variables achter elkaar in het geheugen word geplaast.
    // hierdoor kan er direct uitgelzen worden
    // werkt mogleijk alleen met de GCC compiler
    char BS_jmpBoot[3];        // intel x86 JMP instruction
    char BS_OEMName[8];        // 8 karacter naam
    uint16_t BPB_bytsPerSec;   // aantal bytes per sectoren
    uint8_t BPB_SecPerClus;    // aantal sectoren per culster
    uint16_t BPB_RscdSecCnt;   // aantal gerserveerde sectoren
    uint8_t BPB_NumFats;       // aatnal FAT structuren (moet altijd 2 zijn of niet)
    uint16_t BPB_RootEntCount; // aantal 32 byte directories altijd 0 voor fat 32
    uint16_t BPB_TotSec16;     // aantal sectoren voor een FAT12 of FAT16 volume, 0 voor FAT 32
    uint8_t BPB_Media;         // media type moet 0xF0 of (0xF8 t/m 0xFF) zijn
    uint16_t BPB_FATSz16;      // grote van 1 FAT voor een FAT12/16 volume, 0 voor FAT32
    uint16_t BPB_SecPerTrack;  // aantal sectoren per track, nodig voor bijv HDD's of floppy's
    uint16_t BPB_NumHeads;     // aantal lees/schrijf hoofden,nodig voor HDD of floppy's
    uint32_t BPB_hiddSec;      // aantal hidden secotren voor de paritie waar
    uint32_t BPB_TotSec32;     // aantal sectoren in 32 bit formaat(geeft niet aan of het een FAT32 systeem si)
    uint32_t BPB_FATSz32;      // FAT32 size van 1 FAT
    uint16_t BPB_ExtFlags;     // Extflags info over of de FAT gemirrod word
    uint16_t BPB_FSVer;        // FAT32 versie nummer
    uint32_t BPB_RootClus;     // het cluster nummer van het eerste cluster van de root directory (meestal 2)
    uint16_t BPB_FSInfo;       // sectornummer van het FSINFO struct in het reserveerde gebied.(meestal 1)
    uint16_t BPB_BkBootSec;    // ????? meestal 6
    uint8_t reserved[12];
    uint8_t BS_DrvNum;     // drive number
    uint8_t reserved2;     // moet 0 zijn
    uint8_t BS_BootSig;    //extended boot sigunature(0x29)
    uint32_t BS_VolID;     // SerieelNummer
    char BS_VolLab[11];    // naam
    char BS_FilSysType[8]; // systemType
  } bootSector;



public:
  struct FileInfo{
  std::string name;
  uint32_t sizeInBytes;
};






  Fat32FileSystem(SerialInterFaceDriver *driver);
  virtual ~Fat32FileSystem();
  void ReadFile(std::string Filename, uint8_t *buffer);
  void StreamFile(std::string Filename, uint32_t *buffer, uint32_t startpoint, uint32_t size);
   void StreamFile(FileInfo Filename, uint32_t *buffer, uint32_t startpoint, uint32_t size);
  void listAllFilesWithExtension(std::string Extension,FileInfo *StructArray,uint32_t maxcountoffiles);


private:
  void getFileInfo(std::string Filename);
  int32_t FindFile(std::string Filename,fileStruct* file);
  uint32_t countAllextension(std::string Extension);
  void getFileSystemInfo();
  void FindFirstDataCluster();
  void getDataFromFile(fileStruct file,uint32_t *buffer, uint32_t startpoint, uint32_t count);
 void getDataFromFileOptimezed(fileStruct file,uint32_t *buffer, uint32_t startpoint, uint32_t count);
  FATChain findFATClusters(fileStruct file);
  FATChain findFATClusters(uint32_t clusternummer);
  int getBufferFat(uint32_t sector); // 
  void AddFATtoBuffer(uint32_t *Buffer,bufferdFATSectorType FAT);
  void AddFATtoBuffer(uint32_t *Buffer,uint32_t sector);
  
  void getFatSector(uint32_t *Buffer,uint32_t sector);


  //void 
   
  
  std::map<std::string,fileStruct> FileBuffer;

  SerialInterFaceDriver *driver;
  uint32_t BootSectorL=0; // locatie van de bootsector
  uint32_t RootDataSectorL=0; // locatie van RootDataSector (voor fat32 altijd 0)
  uint32_t FirstDataSectorL=0; //locatie van de root directorie
  uint32_t DataSecCount=0;     // aantal besckibare datasectoren mogelijk onnodig
  uint32_t DataClusterCount=0;
  uint32_t clusterSize=0;
  const int FatBufferSize =15;
  bufferdFATSectorType *BufferdFAT;//[FatBufferSize]={0};
 
};
