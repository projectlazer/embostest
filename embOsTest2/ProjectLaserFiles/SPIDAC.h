

#pragma once
#include "DacDriver.h" 
#include "SerialInterFaceDriver.h" // Spi interface omdat het een SPI DAC is
// DAC driver dat 2 dezelfde DAC's aanstuurt.
// Dit kan zijn door een IC dat 2 of meer DAC's bevat, een MCU dat 2 DAC's bevat
// of 2 losse DAC IC's
class SPIDAC: public DacDriver{
public:
SPIDAC();
SPIDAC(DACtypes DriverType);
virtual ~SPIDAC();

void writeAnalog(uint16_t analogValue, uint8_t dacnumber);
void SetHAlDriver(void* Driver);
private:
  
  SerialInterFaceDriver *Interface;

  static SPIDAC Own; 

};