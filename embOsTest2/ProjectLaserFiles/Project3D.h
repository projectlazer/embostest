#include "LaserStructs.h"
#include <math.h>
#include <stdint.h>

class Project3D {
public:
  Project3D();
  virtual ~Project3D();
  typedef struct mat4x4 {
    float m[4][4] = {0};
  };
  typedef struct vec3d {
    float x;
    float y;
    float z;
  };

  mat4x4 matProj;
  mat4x4 matRotZ;
  mat4x4 matRotX;
  mat4x4 matRotY;
  // Projection Matrix

  const float PI = 3.1415927;

  const float fNear = 0.1f;
  const float fFar = 32767.0f * 2;
  const float fFov = 90.0f;
  const float fAspectRatio = 1;
  const float fFovRad = 1.0f / tanf(fFov * 0.5f / 180.0f * PI);


      private : 
      void MultiplyMatrixVector(vec3d &i, vec3d &o, mat4x4 &m);

public:
  LaserPos project(LaserPos Laserpos);
  void setRotationZ(float fTheta);
  void setRotationY(float fTheta);
  void setRotationX(float fTheta);
};