#include "ExternalStorageFac.h"

ExternalStorageFac::storagemap &ExternalStorageFac::getStorageMap() {
  static storagemap map;
  return map;
}

ExternalStorage *ExternalStorageFac::MakeStorage(StrorageType type) {
  storagemap &lookup = getStorageMap();
  ExternalStorage *temp = lookup.find(type)->second->clone();

  return temp;
}

ExternalStorage *ExternalStorageFac::MakeStorage(StrorageType type, SerialInterFaceDriver *interface) {
  storagemap &lookup = getStorageMap();
  ExternalStorage *temp = lookup.find(type)->second->clone(interface);

  return temp;
}

void ExternalStorageFac::AddStorage(StrorageType type, ExternalStorage *storage) {
  storagemap &lookup = getStorageMap();
  lookup.insert(std::pair<StrorageType, ExternalStorage *>(type, storage));
}