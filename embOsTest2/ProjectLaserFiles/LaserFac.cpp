#include "LaserFac.h"

void LaserFac::choseLaser(LaserDriveType type, LaserDriver *laserdriver) {

  LaserDriverMap &temp = getLaserMap();
  temp.insert(std::pair<LaserDriveType, LaserDriver *>(type, laserdriver));
}

LaserDriver *LaserFac::makelaser(LaserDriveType type) {
  //LaserDriver* tempLaserDriver = getLaserMap();
  LaserDriverMap &lookup = getLaserMap();
  LaserDriver *temp; // = lookup.find(type)->second;
  return temp;
}

LaserFac::LaserDriverMap &LaserFac::getLaserMap() {

  static LaserDriverMap returntype;
  return returntype;
}

LaserFac::LaserConfigMap &LaserFac::getConfigMap() {
  static LaserConfigMap returntype;
  return returntype;
}

LaserDriver *LaserFac::makeConfig(LaserConfigs config) {

  LaserConfigMap &lookup = getConfigMap();
  LaserDriver *temp = lookup.laserMap.find(config.laser)->second;
  temp->setDac(lookup.DCMap.find(config.Dac)->second,config.HalDriver);
  temp->setPWM(lookup.PWMap.find(config.PWM)->second);
  

  return temp;
}
void LaserFac::addDac(DACtypes type, DacDriver *Driver) {

 LaserConfigMap &lookup = getConfigMap();
 lookup.DCMap.insert({type,Driver});
}
void LaserFac::addPWM(PWMTypes type, PWMDriver *Driver) {
LaserConfigMap &lookup = getConfigMap();
 lookup.PWMap.insert({type,Driver});
}
void LaserFac::addLaser(LaserType type, LaserDriver *Driver) {
LaserConfigMap &lookup = getConfigMap();
 lookup.laserMap.insert({type,Driver});
}