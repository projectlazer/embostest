#include "IldaPharser.h"

IldaPharser::IldaPharser() {
}

IldaPharser::~IldaPharser() {
}

uint32_t IldaPharser::getBytesPerRecord(IldaHeader header) {

  return formatsize[header.FormatCode];
}

uint32_t IldaPharser::parseStreamData(uint8_t *data, LaserCoord *parsedData, IldaHeader IldaFormat, uint32_t datacount) {
  uint32_t countofpoints = 0;
  //count of points zijn altijd 0 voor de optimiser. De waarschuwing van geen return waarden zorgt voor een hardfault
  // omdat de count of points nooit gebruikt word is dit nu een snelle oplossing
  switch (IldaFormat.FormatCode) {
  case 0:
    countofpoints = parseFormat0(data, parsedData, datacount * format0Size);
    break;
  case 1:
    countofpoints = parseFormat1(data, parsedData, datacount * format1Size);
    break;
  case 4:
    countofpoints = parseFormat4(data, parsedData, datacount * format4Size);
    break;
  case 5:
    countofpoints = parseFormat5(data, parsedData, datacount * format5Size);
    break;
  }
  return countofpoints;
}

uint32_t IldaPharser::parseFormat0(uint8_t *data, LaserCoord *parsedData, uint32_t count) {
  uint8_t tempcolorpos = 0;
  uint8_t tempColorEnable = 0;
  volatile uint32_t count2 = count;

  for (int i = 0; i < (count / format0Size); i++) {
    parsedData->Pos.x = *data << 8;
    data++;
    parsedData->Pos.x |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.y = *data << 8;
    data++;
    parsedData->Pos.y |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.z = *data << 8;
    data++;
    parsedData->Pos.z |= (*data); //mischien gaat dit goed
    data++;
    tempColorEnable = *data;
    data++;
    tempcolorpos = *data;
    data++;

    uint8_t ColorEnable = ((tempColorEnable >> 6) & 1) ^ 1;
    //parsedData->Pos.x ^= 0x8000;
   // parsedData->Pos.y ^= 0x8000;
   // parsedData->Pos.z ^= (~parsedData->Pos.z)+1;
    parsedData->Color.blue = colortable[tempcolorpos].blauw * ColorEnable;
    parsedData->Color.green = colortable[tempcolorpos].groen * ColorEnable;
    parsedData->Color.red = colortable[tempcolorpos].rood * ColorEnable;
    parsedData++;
  }
  return 0;
}
uint32_t IldaPharser::parseFormat1(uint8_t *data, LaserCoord *parsedData, uint32_t count) {
  uint8_t tempcolorpos = 0;
  uint8_t tempColorEnable = 0;
  for (int i = 0; i < (count / format1Size); i++) {
    parsedData->Pos.x = *data << 8;
    data++;
    parsedData->Pos.x |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.y = *data << 8;
    data++;
    parsedData->Pos.y |= (*data); //mischien gaat dit goed
    data++;

    tempColorEnable = *data;
    data++;

    tempcolorpos = *data;
    data++;

    uint8_t ColorEnable = ((tempColorEnable >> 6) & 1) ^ 1;
    //parsedData->Pos.x ^= 0x8000;
   // parsedData->Pos.y ^= 0x8000;
    parsedData->Color.blue = colortable[tempcolorpos].blauw * ColorEnable;
    parsedData->Color.green = colortable[tempcolorpos].groen * ColorEnable;
    parsedData->Color.red = colortable[tempcolorpos].rood * ColorEnable;
   parsedData->Pos.z=0;
    parsedData++;
  }
  return 0;
}
uint32_t IldaPharser::parseFormat4(uint8_t *data, LaserCoord *parsedData, uint32_t count) {
  uint8_t tempcolorpos = 0;
  uint8_t tempColorEnable = 0;
  for (int i = 0; i < (count / format4Size); i++) {
    parsedData->Pos.x = *data << 8;
    data++;
    parsedData->Pos.x |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.y = *data << 8;
    data++;
    parsedData->Pos.y |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.z = *data << 8;
    data++;
    parsedData->Pos.z |= (*data); //mischien gaat dit goed
    data++;

    tempColorEnable = *data;
    data++;
    uint8_t ColorEnable = ((tempColorEnable >> 6) & 1) ^ 1;

    parsedData->Color.blue = (*data) * ColorEnable;
    data++;
    parsedData->Color.green = (*data) * ColorEnable;
    data++;
    parsedData->Color.red = (*data) * ColorEnable;
    data++;

    //parsedData->Pos.x ^= 0x8000;
    //parsedData->Pos.y ^= 0x8000;
    //parsedData->Pos.z ^= (~parsedData->Pos.z)+1;

    parsedData++;
  }
  return 0;
}
uint32_t IldaPharser::parseFormat5(uint8_t *data, LaserCoord *parsedData, uint32_t count) {
  uint8_t tempcolorpos = 0;
  uint8_t tempColorEnable = 0;
  for (int i = 0; i < (count / format5Size); i++) {
    parsedData->Pos.x = *data << 8;
    data++;
    parsedData->Pos.x |= (*data); //mischien gaat dit goed
    data++;

    parsedData->Pos.y = *data << 8;
    data++;
    parsedData->Pos.y |= (*data); //mischien gaat dit goed
    data++;

    tempColorEnable = *data;
    data++;

    uint8_t ColorEnable = ((tempColorEnable >> 6) & 1) ^ 1;

    parsedData->Color.blue = (*data) * ColorEnable;
    data++;
    parsedData->Color.green = (*data) * ColorEnable;
    data++;
    parsedData->Color.red = (*data) * ColorEnable;
    data++;

   // parsedData->Pos.x ^= 0x8000;
   // parsedData->Pos.y ^= 0x8000;
  
   parsedData->Pos.z=0;

    parsedData++;
  }
  return 0;
}

IldaHeader IldaPharser::getIldaHeader(uint32_t data[8]) {
  IldaHeader temp;
  //temp = (IldaHeader )data; //wat is misra compliance
  memcpy(&temp, data, sizeof(IldaHeader));
  temp.NumberOfRecords = ((temp.NumberOfRecords >> 8) & 0xFF) | ((temp.NumberOfRecords & 0xFF) << 8);
  temp.totalFrames = ((temp.totalFrames >> 8) & 0xFF) | ((temp.totalFrames & 0xFF) << 8);
  temp.FrameNumber = ((temp.FrameNumber >> 8) & 0xFF) | ((temp.FrameNumber & 0xFF) << 8);

  return temp;
}