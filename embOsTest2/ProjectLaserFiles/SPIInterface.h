
#include "SerialInterFaceDriver.h"
#include "stm32h7xx_hal.h"
struct SlaveSelectStruct {
  GPIO_TypeDef *GPIOHanndle;
  uint16_t GPIO_Pin;
  uint8_t slaveactivestate : 1;
};

struct SPIANDGPIOstruct {
  SPI_HandleTypeDef *SpiHandle;
  SlaveSelectStruct SlaceSelectArray[8];
};






class SPIInterface : public SerialInterFaceDriver {
#define BUFFERSIZE 63
  //als de hal driver gebruikt word maken de verschillende writedata's uit.
  //addres is in dit geval de chipselect
public:
  static SPIInterface Own;
  SPIInterface();
  virtual ~SPIInterface();

  void SPIIRQHandler();

  void writeData(uint32_t addres, uint8_t *data, uint32_t count); //32bit count
  void writeData(uint32_t addres, uint16_t *data, uint32_t count);
  void writeData(uint32_t addres, uint32_t *data, uint32_t count);
  void* ReadData(uint32_t addres, uint32_t *data, uint32_t count); //lezen word altijd naar een 32 bit data pointer gedaan.
  void* ReadData(uint32_t addres, uint8_t *data, uint32_t count); //lezen word altijd naar een 32 bit data pointer gedaan.
  SPIInterface *clone();
  void addHalDriver(void *HalDriver);
  void testfucntion();
  int8_t addHalDriver(void *HalDriver , bool test); // voeg een driver toe aan de array en geef een getal terug met waar deze geplaast is

public:

struct IRQBuffertype{
SPIANDGPIOstruct *SpiandSlave;
uint32_t addres;
uint32_t* data;
uint32_t count;
uint32_t sizeofDataType; 

};




  SPIANDGPIOstruct *testStruct = nullptr;

  SPIANDGPIOstruct *SPISelectStruct[8]={0}; // 8 SPIinterfaces
  int8_t SelectStructCount=0;

  void selectSlave(SlaveSelectStruct slave);
  void deselectSlave(SlaveSelectStruct slave);
  
  void addIRQType(SPIANDGPIOstruct *SPIAndSlave, uint32_t *data);

  void DisableInterrupt(SPI_HandleTypeDef *SpiHandle);
  void EnableInterrupt(SPI_HandleTypeDef *SpiHandle);



  volatile uint8_t Writepos=0;
  volatile uint8_t Readpos=0;
  IRQBuffertype Buffer[BUFFERSIZE+1]; // geen OS queue om porten eenvoudig te houden
  

  SPI_HandleTypeDef *SpiHandle = nullptr;

};