
#include <string>
#include "stdint.h"
#include "Fat32FileSystem.h"
// een abstractie laag voor een bestand.
// een file bied de mogelijkheid om een speciefiek aantal bytes van een file te lezen zonder rekening te houden met biojvoorbleed clustergrote van een filesystem.
// het leest aan de hand van het aantal gevraagde bytes en slaat deze tijdelijk op.
// hierdoor hoefd het filesysteem of het filegebruiker geen buffer te maken.
// als er meer gelezen gewenst te worden handelt de file klasse dit af. 
class File{

public:
  File();
  File(std::string filename);
  ~File();
  void openFile(std::string filename);
  void ReadBytes(uint8_t *buffer, uint32_t startpos , uint32_t count);
  void ReadBytes(uint16_t *buffer, uint32_t startpos , uint32_t count);
  void ReadBytes(uint32_t *buffer, uint32_t startpos , uint32_t count);
  void seek(uint32_t pos); // zet read/write lees pointer
 
  uint32_t getFileSize();
private:


std::string name;
uint32_t sizeinbytes;
  uint32_t buffersizeinbytes=32768;// 32kiB buffer
  uint32_t buffer[32768>>2]; 
  uint32_t readWritePos=0;

  
};