#include "Laser.h "
#include "LaserFac.h"
//Laser::Laser() {
//}

Laser::Laser(LaserConfigs config, Fat32FileSystem *system) {
  Driver = LaserFac::makeConfig(config);
  this->filesystem = system;
  this->ilda = new IldaPharser();
  this->Projecttor = new Project3D();
  this->Projecttor->setRotationX(1.0f);


  this->testPrescaler(0);
}
void Laser::testpoint2(LaserCoord *Coord) {

  LaserCoord temp = *Coord;
  temp.Pos = this->Projecttor->project(temp.Pos);

  this->Driver->writePoint(temp);
  // CoordBuffer[1000][0]=*Coord;
}

void Laser::nextIlda() {
}
void Laser::prevIlda() {
}
void Laser::selectIlda(std::string FileName) {

  this->filesystem->StreamFile(FileName, this->buffer, 0, 512);
  this->CurrentHeader = this->ilda->getIldaHeader(buffer);
}
void Laser::selectMode(LaserModes Mode) {

  this->Mode = Mode;
}
void Laser::nextPoint() {
  nextpointRequested = true;
  readpos++;
  this->Driver->writePoint(CoordBuffer[readpos % BUFFERSIZE]);
}
void Laser::setRotation(float x, float y, float z) {
  this->Projecttor->setRotationX(x);
  this->Projecttor->setRotationY(y);
  this->Projecttor->setRotationZ(z);
}

void Laser::testPrescaler(uint16_t prescalewaarde) {

  this->Driver->pwm->writePrescaler(prescalewaarde);
}

void Laser::IoHandler(){



}




Laser::~Laser() {

  delete this->ilda;
}