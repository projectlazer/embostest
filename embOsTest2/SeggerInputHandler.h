#pragma once
#include "SEGGER_RTT.h"
#include "stdint.h"
#include <functional>
#include <string>
#include <vector>





class SeggerInputHandler {

public:

  typedef struct {

    std::string commandName; // naam die word gebruik voor de aanroep via UART
    uint8_t CommandSize;
    void* instance;
    void (*CommandFunction)(std::string restofInput); // functie pointer naar de functie toe
    std::string Omschrijving;                   //Optionele omschrijving

  } commandStruct;


  SeggerInputHandler();
  virtual ~SeggerInputHandler();

  void getInput();
  void addFunction(void (*functie)(std::string restofInput), std::string commandName);
  void addFunction(void (*functie)(std::string restofInput), std::string commandName, std::string help);
  void addFunction(commandStruct Command);
 

private:
  void textInterperter(std::string input);
  
  std::vector<commandStruct> CommandStructs;
  std::string input;
  uint8_t writeLoc = 0;
  uint8_t inputSize = 0;
  uint8_t inputLoc=0;
  char delimiter = ' ';
};