# embOSTest

Test voor embOS van SEGGER.
embOS is een licht gewicht OS.

Vanwegen problemen met het interupten van een timer(het enable interupt bit gaf een hardfault) wat direct te linken was een een .s bestand gemaakt in cubeMX si een nieuw project gemaakt buiten cubeMx om.
De HAL drivers moeten dan handmatig worden toegevoegt.
Omdat enbedded studio een optie bied om een OS applicatie te maken is er voor gekozen om dit te proberen.
meer info op :
https://www.segger.com/products/rtos/embos/

Mogelijk problemen:
1. 
    De HAL driver maakt gebruik van de systick als timing bron
        Omdat de systick bedoelt is voor OS doeleindes gebruikt het embOS dezefde timer.
        Confilcten kunnen ontstaan als de HAL driver veranderingen toepast op de systick register.
2.
    het project is gemaakt voor de STM32H743I-eval board.
        geeft mogelijke problemen als het eval board speciale dingen met het OS doet.
